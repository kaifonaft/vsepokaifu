
var items=new Array('sword','back','helmet','shoes','armor','shield','necklace','ring1','ring2');
var modification={'sword':0,'back':0,'helmet':0,'shoes':0,'armor':0,'shield':0,'necklace':0,'ring1':0,'ring2':0};
var selected_arts={'sword_s':0,'back_s':0,'necklace_s':0,'ring1_s':0,'ring2_s':0,'shoes_s':0,'shield_s':0,'helmet_s':0,'armor_s':0};
var sword_base=new Array();
var back_base=new Array();
var helmet_base=new Array();
var shoes_base=new Array();
var armor_base=new Array();
var shield_base=new Array();
var necklace_base=new Array();
var ring1_base=new Array();
var ring2_base=new Array();


var nArr=new Array();

/*name,image,price,cost_per_battle,durability,oa,lvl,link,attack,deff,sm,kn,ini,luck,bd,resist,f_resist,e_resist,a_resist,w_resist,add_close_attack,def_close_attack,def_far_attack,add_far_attack,probiv,hero_ini, use_in_search,art_id*/
var empty_item = new Array('','_s.jpg',0,0,0,0,0,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0);

var ring1=new Array(new Array('Кольцо ловкости','i_ring_s.jpg',180,18,10,1,2,'i_ring',0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,237),new Array('Перстень вдохновения','eaglering_s.jpg',1660,89.44,18,2,4,'verve_ring',0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,216),new Array('Кольцо силы','sring4_s.jpg',610,40.67,15,2,4,'sring4',1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,421),new Array('Кольцо сомнений','necroring_s.jpg',1120,90.5,12,2,4,'doubt_ring',0,0,0,0,0,1,-2,0,0,0,0,0,0,0,0,0,0,0,1,217),new Array('Кольцо стремительности','hastering_s.jpg',2030,65.63,30,2,5,'rashness_ring',0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1,218),new Array('Кольцо отречения','circ_ring_s.jpg',6850,132.88,50,4,6,'circ_ring',0,-1,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,1,219),new Array('Кольцо пророка','powerring_s.jpg',5460,132.4,40,4,7,'powerring',0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,220),new Array('Терновое кольцо','sring10_s.jpg',3010,100.33,30,5,10,'sring10',2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,422),new Array('Кольцо молнии','smring10_s.jpg',3010,100.33,30,5,10,'smring10',0,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,427),new Array('Кольцо воина','warriorring_s.jpg',7050,170.95,40,5,10,'warriorring',3,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1,222),new Array('Кольцо теней','darkring_s.jpg',8820,171.1,50,5,10,'darkring',0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,221),new Array('Печать заклинателя','magring13_s.jpg',10820,174.92,60,6,13,'magring13',0,0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,241),new Array('Глаз дракона','warring13_s.jpg',10820,174.92,60,6,13,'warring13',3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,1,240),new Array('Кольцо противоречий','bring14_s.jpg',10920,176.53,60,6,14,'bring14',1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,242),new Array('Кольцо звёзд','mmmring16_s.jpg',11830,176.54,65,6,16,'mmmring16',0,0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,5,0,1,244),new Array('Кольцо боли','wwwring16_s.jpg',11830,176.54,65,6,16,'wwwring16',3,2,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,243),new Array('Кольцо хватки дракона','fgsring17_s.jpg',3060,102,30,6,17,'sring17',2,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,463),new Array('Печать единения','masmring17_s.jpg',3060,102,30,6,17,'smring17',0,2,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,464),new Array('Кольцо непрестанности','meqmring19_s.jpg',11990,184.46,65,7,19,'mring19',0,1,2,1,0,0,0,0,0,0,0,0,0,0,0,0,6,0,1,461),new Array('Кольцо бесстрашия','rarring19_s.jpg',11900,183.08,65,7,19,'ring19',3,3,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,462));
var shield=new Array(new Array('Круглый щит','roundshield_s.jpg',110,15.71,7,1,1,'round_shiled',0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,180),new Array('Стальной щит','s_shield_s.jpg',280,18.67,15,2,2,'s_shield',0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,212),new Array('Кинжал мести','dagger_s.jpg',1680,31.03,30,1,3,'dagger',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,181),new Array('Щит хранителя','protectshield_s.jpg',1190,28.85,40,3,4,'defender_shield',0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,182),new Array('Щит славы','sshield5_s.jpg',3040,76,40,4,5,'sshield5',0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,417),new Array('Щит драконов','dragon_shield_s.jpg',9240,128.03,70,5,7,'dragon_shield',1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,183),new Array('Свиток энергии','energy_scroll_s.jpg',9520,131.91,70,6,10,'energy_scroll',0,0,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,185),new Array('Башенный щит','large_shield_s.jpg',10080,139.67,70,6,10,'large_shield',0,5,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,1,184),new Array('Щит сокола','sshield11_s.jpg',4080,102,40,6,11,'sshield11',0,4,0,0,1,0,0,0,0,0,0,0,0,0,5,0,0,0,1,418),new Array('Обсидиановый щит','shield13_s.jpg',10710,148.4,70,7,13,'shield13',1,4,0,0,0,0,0,0,0,0,0,0,0,0,10,0,0,0,1,214),new Array('Щит чешуи дракона','zpsshield14_s.jpg',4130,108.68,38,7,14,'sshield14',0,5,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0,1,457),new Array('Щит пламени','shield16_s.jpg',10840,150.2,70,8,16,'shield16',0,5,0,0,0,0,0,0,0,0,0,0,0,0,15,0,0,0,1,213),new Array('Щит подавления','esshield17_s.jpg',4230,120.86,35,8,17,'sshield17',1,4,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,1,456),new Array('Манускрипт концентрации','shhscroll18_s.jpg',10850,155,70,9,18,'scroll18',0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,5,0,1,438),new Array('Щит рассвета','sioshield19_s.jpg',11020,157.43,70,9,19,'shield19',0,6,0,0,0,0,0,0,0,0,0,0,0,0,17,0,0,0,1,455));
var shoes=new Array(new Array('Кожаные ботинки','leatherboots_s.jpg',210,15,14,1,1,'leatherboots',0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,140),new Array('Кожаные сапоги','hunterboots_s.jpg',960,31.03,30,1,4,'hunter_boots',0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,113),new Array('Туфли стремления','initboots_s.jpg',2510,60.85,40,3,5,'shoe_of_initiative',0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,1,114),new Array('Боевые сапоги','boots2_s.jpg',1080,30.86,35,2,5,'boots2',0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,141),new Array('Стальные сапоги','steel_boots_s.jpg',6090,84.39,70,4,7,'steel_boots',0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,115),new Array('Лёгкие мифриловые сапоги','mif_lboots_s.jpg',7530,132.8,55,6,8,'mif_lboots',0,3,0,0,2,0,0,5,0,0,0,0,0,0,0,0,0,0,1,116),new Array('Солдатские сапоги ','sboots9_s.jpg',2250,75,30,5,9,'sboots9',0,2,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,1,419),new Array('Тяжёлые мифриловые сапоги','mif_hboots_s.jpg',8160,121.77,65,6,11,'mif_hboots',0,5,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1,130),new Array('Рубиновые сапоги','sboots12_s.jpg',3150,90,35,6,12,'sboots12',0,4,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,1,420),new Array('Туфли чародея','wiz_boots_s.jpg',8430,125.8,65,6,12,'wiz_boots',0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,1,133),new Array('Обсидиановые сапоги','boots13_s.jpg',8950,124.01,70,7,13,'boots13',0,5,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,1,143),new Array('Сапоги чернокнижника','mboots14_s.jpg',9290,128.73,70,8,14,'mboots14',0,3,1,0,0,0,0,5,0,0,0,0,0,0,0,0,0,10,1,144),new Array('Сапоги пламени','boots15_s.jpg',9010,124.84,70,8,15,'boots15',0,5,0,0,0,0,0,7,0,0,0,0,0,3,0,0,0,0,1,145),new Array('Сапоги благородства','nmsboots16_s.jpg',3410,113.67,30,8,16,'sboots16',0,4,0,0,1,0,0,5,0,0,0,0,0,0,0,0,0,0,1,460),new Array('Сапоги сумерек','macmboots17_s.jpg',9140,130.57,70,9,17,'mboots17',0,4,1,0,0,0,0,10,0,0,0,0,0,0,0,0,0,10,1,458),new Array('Сапоги рассвета','bzbboots17_s.jpg',9140,130.57,70,9,17,'boots17',0,5,0,0,0,0,0,9,0,0,0,0,0,6,0,0,0,0,1,459));
var sword=new Array(new Array('Деревянный меч','woodensword_s.jpg',140,20,7,1,1,'wood_sword',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1),new Array('Легкий топорик','onehandaxe_s.jpg',310,12,25,2,2,'gnome_hammer',2,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2),new Array('Стальной клинок','steelsword_s.jpg',670,15.83,30,2,3,'steel_blade',2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,3),new Array('Меч расправы','def_sword_s.jpg',1319,32.975,40,3,3,'def_sword',2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,4),new Array('Меч возмездия','requitalsword_s.jpg',2660,64.5,40,5,5,'requital_sword',3,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,5),new Array('Боевой посох ','staff_s.jpg',2660,64.5,40,6,5,'staff',1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,6),new Array('Меч равновесия','broadsword_s.jpg',4970,80.33,60,6,6,'broad_sword',2,2,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1,7),new Array('Посох могущества','sor_staff_s.jpg',6440,124.92,50,8,7,'sor_staff',0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,9),new Array('Меч власти','power_sword_s.jpg',10290,124.76,80,8,7,'power_sword',5,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,1,8),new Array('Посох весны','mstaff8_s.jpg',3040,101.33,30,8,8,'mstaff8',0,1,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,423),new Array('Меч жесткости','ssword8_s.jpg',4040,101,40,8,8,'ssword8',4,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1,414),new Array('Мифриловый меч','mif_sword_s.jpg',17850,247.34,70,9,9,'mif_sword',6,0,0,0,2,0,0,0,0,0,0,0,5,0,0,0,0,0,1,10),new Array('Мифриловый посох','mif_staff_s.jpg',17250,239.03,70,9,9,'mif_staff',1,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,11),new Array('Посох теней','mstaff10_s.jpg',3980,113.71,35,9,10,'mstaff10',1,1,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,424),new Array('Меч отваги','ssword10_s.jpg',5110,113.56,45,9,10,'ssword10',6,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,1,415),new Array('Рубиновый посох','mm_staff_s.jpg',17343,247.76,70,10,12,'mm_staff',0,1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,10,0,1,36),new Array('Рубиновый меч','mm_sword_s.jpg',18100,250.81,70,10,12,'mm_sword',7,0,0,0,1,0,0,0,0,0,0,0,7,0,0,0,0,0,1,35),new Array('Обсидиановый посох','mstaff13_s.jpg',5050,126.25,40,10,13,'mstaff13',0,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,425),new Array('Обсидиановый меч','ssword13_s.jpg',6300,126,50,10,13,'ssword13',7,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,1,416),new Array('Посох повелителя огня','ffstaff15_s.jpg',18610,257.87,70,11,15,'ffstaff15',1,0,2,3,0,0,0,0,0,0,0,0,0,0,0,0,10,0,1,49),new Array('Меч возрождения','firsword15_s.jpg',18600,257.74,70,11,15,'firsword15',8,0,0,0,0,0,0,0,0,0,0,0,9,0,0,0,0,0,1,48),new Array('Меч гармонии','szzsword16_s.jpg',6370,138.48,46,11,16,'ssword16',7,1,0,0,0,0,0,0,0,0,0,0,6,0,0,0,0,0,1,441),new Array('Посох забвения','ssmstaff16_s.jpg',5140,138.92,37,11,16,'smstaff16',0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,442),new Array('Гладий предвестия','smasword18_s.jpg',18690,267,70,12,18,'sword18',8,1,0,0,0,0,0,0,0,0,0,0,10,0,0,0,0,0,1,437),new Array('Посох затмения','smmstaff18_s.jpg',18680,266.86,70,12,18,'staff18',1,2,3,2,0,0,0,0,0,0,0,0,0,0,0,0,11,0,1,439));
var back=new Array(new Array('Короткий лук','shortbow_s.jpg',360,18,20,1,4,'shortbow',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,1,77),new Array('Плащ разведчика','cloack_s.jpg',320,16,20,1,4,'scoutcloack',0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,1,78),new Array('Накидка духов','soulcape_s.jpg',1260,40.73,30,2,5,'soul_cape',0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,52),new Array('Длинный лук','long_bow_s.jpg',6650,129,50,4,6,'long_bow',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,0,0,1,51),new Array('Халат ветров','antiair_cape_s.jpg',3080,49.78,60,3,6,'antiair_cape',0,0,0,0,0,0,0,0,0,0,25,0,0,0,0,0,0,0,1,53),new Array('Халат магической защиты','antimagic_cape_s.jpg',5210,101.06,50,5,8,'antimagic_cape',0,0,0,0,0,0,0,15,0,0,0,0,0,0,0,0,0,0,1,55),new Array('Плащ магической силы','powercape_s.jpg',5620,136.28,40,4,8,'powercape',0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,54),new Array('Маскировочный плащ','scloack8_s.jpg',2160,72,30,4,8,'scloack8',0,0,0,0,0,0,0,0,0,0,0,0,0,0,12,0,0,0,1,413),new Array('Составной лук','composite_bow_s.jpg',8680,153.07,55,5,11,'composite_bow',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,15,0,0,1,67),new Array('Накидка чародея','wiz_cape_s.jpg',9170,148.23,60,7,12,'wiz_cape',0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,15,0,1,68),new Array('Лук полуночи','bow14_s.jpg',10470,156.23,65,6,14,'bow14',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,18,0,0,1,79),new Array('Мантия пламенного чародея','cloackwz15_s.jpg',10120,151.02,65,8,15,'cloackwz15',0,0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,15,0,1,80),new Array('Плащ драконьего покрова','mascloack16_s.jpg',3360,112,30,8,16,'scloack16',0,1,0,0,0,0,0,0,0,0,0,0,0,0,14,0,0,0,1,454),new Array('Мантия вечности','clscloack17_s.jpg',10500,161.54,65,9,17,'cloack17',0,1,2,1,0,0,0,0,0,0,0,0,0,0,0,0,16,0,1,453),new Array('Лук рассвета','bbobow17_s.jpg',10640,163.69,65,7,17,'bow17',0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,20,0,0,1,440));
var armor=new Array(new Array('Кожаная броня','leathershield_s.jpg',280,15.56,18,1,1,'leather_shiled',0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,146),new Array('Кожаные доспехи','leatherplate_s.jpg',1430,47.67,30,2,3,'leatherplate',0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,176),new Array('Боевая кольчуга','chainarmor_s.jpg',2410,58.43,40,3,5,'hauberk',0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,147),new Array('Стальная кираса','ciras_s.jpg',4690,64.99,70,4,7,'ciras',0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,148),new Array('Одеяние мага','mage_armor_s.jpg',4700,91.18,50,5,8,'mage_armor',0,3,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,149),new Array('Лёгкая мифриловая кираса','mif_light_s.jpg',6580,91.17,70,5,8,'mif_light',0,4,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1,150),new Array('Мифриловая кольчуга','sarmor9_s.jpg',2610,65.25,40,5,9,'sarmor9',0,3,0,0,1,0,0,3,0,0,0,0,0,0,0,0,0,0,1,411),new Array('Стальные доспехи','full_plate_s.jpg',9730,125.84,75,6,10,'full_plate',0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,151),new Array('Роба чародея','mage_robes_s.jpg',9870,136.76,70,7,11,'wiz_robe',0,4,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,166),new Array('Мифриловые доспехи','miff_plate_s.jpg',10360,133.99,75,7,12,'miff_plate',0,5,0,0,0,0,0,5,0,0,0,0,0,5,0,0,0,0,1,169),new Array('Обсидиановая броня','sarmor13_s.jpg',4550,91,50,7,13,'sarmor13',0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,412),new Array('Роба пламенного чародея','robewz15_s.jpg',9800,135.8,70,8,15,'robewz15',0,5,1,1,0,0,0,3,0,0,0,0,0,0,0,0,0,0,1,179),new Array('Доспех пламени','armor15_s.jpg',9800,135.8,70,8,15,'armor15',0,5,0,0,0,0,0,7,0,0,0,0,0,7,0,0,0,0,1,178),new Array('Кираса благородства','brsarmor16_s.jpg',4580,104.09,44,8,16,'sarmor16',0,4,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,452),new Array('Кираса рассвета','anwarmor17_s.jpg',9990,142.71,70,9,17,'armor17',0,5,0,0,0,0,0,9,0,0,0,0,0,9,0,0,0,0,1,450),new Array('Доспехи сумерек','mammarmor17_s.jpg',9800,140,70,9,17,'marmor17',0,5,1,1,0,0,0,10,0,0,0,0,0,0,0,0,0,0,1,451));
var necklace=new Array(new Array('Медаль отваги','braverymedal_s.jpg',590,22.88,25,2,2,'bravery_medal',0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,245),new Array('Амулет удачи','lucknecklace_s.jpg',1010,39.16,25,2,3,'amulet_of_luck',0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,246),new Array('Кулон отчаяния','power_pendant_s.jpg',7770,125.6,60,7,7,'power_pendant',1,1,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,1,247),new Array('Счастливая подкова','samul81_s.jpg',3570,119,30,7,8,'samul8',0,0,0,0,3,1,0,0,0,0,0,0,0,0,0,0,0,0,1,409),new Array('Магический амулет','magic_amulet_s.jpg',8820,171.1,50,7,10,'magic_amulet',0,0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,249),new Array('Кулон воина','warrior_pendant_s.jpg',8470,164.3,50,8,10,'warrior_pendant',3,2,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,1,248),new Array('Мистический амулет','mmzamulet13_s.jpg',10500,169.75,60,9,13,'mmzamulet13',0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,3,0,1,321),new Array('Амулет ярости','wzzamulet13_s.jpg',10500,169.75,60,9,13,'wzzamulet13',3,3,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,1,319),new Array('Осколок тьмы','smamul14_s.jpg',4600,153.33,30,9,14,'smamul14',0,2,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,426),new Array('Амулет фортуны','samul141_s.jpg',4600,153.33,30,9,14,'samul14',1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,410),new Array('Амулет трёх стихий','bafamulet15_s.jpg',11380,169.82,65,9,15,'bafamulet15',2,2,1,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1,323),new Array('Амулет духа','mmzamulet16_s.jpg',11550,172.35,65,10,16,'mmzamulet16',0,0,3,2,0,0,0,0,0,0,0,0,0,0,0,0,5,0,1,322),new Array('Амулет битвы','wzzamulet16_s.jpg',11550,172.35,65,10,16,'wzzamulet16',3,1,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,1,320),new Array('Амулет единения','sekmamul17_s.jpg',4620,154,30,10,17,'smamul17',0,3,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,448),new Array('Оскал дракона','warsamul17_s.jpg',4620,154,30,10,17,'samul17',1,3,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,449),new Array('Кулон рвения','nwamulet19_s.jpg',11620,178.77,65,11,19,'amulet19',3,3,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,1,446),new Array('Кулон непостижимости','megmamulet19_s.jpg',11620,178.77,65,11,19,'mamulet19',0,1,2,3,0,0,0,0,0,0,0,0,0,0,0,0,5,0,1,447));
var helmet=new Array(new Array('Кожаная шляпа','leatherhat_s.jpg',180,15,12,1,1,'leatherhat',0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,107),new Array('Кожаный шлем','leatherhelmet_s.jpg',660,21.33,30,1,3,'leather_helm',0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,81),new Array('Колпак мага','magehat_s.jpg',1680,46.54,35,2,5,'wizard_cap',0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,82),new Array('Шляпа знаний','knowlengehat_s.jpg',1030,39.96,25,2,5,'knowledge_hat',0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,83),new Array('Кольчужный шлем','chaincoif_s.jpg',1620,39.28,40,2,5,'chain_coif',0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,84),new Array('Шлем мага','mage_helm_s.jpg',3450,66.92,50,4,7,'mage_helm',0,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,86),new Array('Стальной шлем','steel_helmet_s.jpg',3870,53.61,70,3,7,'steel_helmet',0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,85),new Array('Шлем отваги','shelm8_s.jpg',1260,42,30,3,8,'shelm8',0,2,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,407),new Array('Лёгкий мифриловый шлем','mif_lhelmet_s.jpg',5520,76.49,70,5,9,'mif_lhelmet',0,2,0,0,1,0,0,5,0,0,0,0,0,0,0,0,0,0,1,87),new Array('Тяжёлый мифриловый шлем','mif_hhelmet_s.jpg',6630,91.87,70,5,11,'mif_hhelmet',0,4,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1,99),new Array('Рубиновый шлем','shelm12_s.jpg',2800,70,40,5,12,'shelm12',0,3,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1,408),new Array('Корона чернокнижника','mhelmetzh13_s.jpg',6720,93.11,70,6,13,'mhelmetzh13',0,3,1,1,0,0,0,3,0,0,0,0,0,0,0,0,0,0,1,110),new Array('Обсидиановый шлем','zxhelmet13_s.jpg',6720,93.11,70,6,13,'zxhelmet13',0,5,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1,109),new Array('Шлем пламени','myhelmet15_s.jpg',6930,96.03,70,7,15,'myhelmet15',0,5,0,0,0,0,0,7,0,0,0,0,0,2,0,0,0,0,1,111),new Array('Корона пламенного чародея','xymhelmet15_s.jpg',6960,96.44,70,7,15,'xymhelmet15',0,2,1,2,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1,112),new Array('Шлем благородства','umshelm16_s.jpg',2920,73,40,7,16,'shelm16',0,4,0,0,1,0,0,5,0,0,0,0,0,0,0,0,0,0,1,444),new Array('Шлем сумерек','miqmhelmet17_s.jpg',7620,108.86,70,8,17,'mhelmet17',0,3,1,2,0,0,0,10,0,0,0,0,0,0,0,0,0,0,1,445),new Array('Шлем рассвета','hwmhelmet17_s.jpg',7620,108.86,70,8,17,'helmet17',0,5,0,0,0,0,0,9,0,0,0,0,0,5,0,0,0,0,1,443));
var items_list = [sword, ring1, shield, shoes, back, armor, necklace, helmet];
for(i in items_list){
    items_list[i].sort(function(a,b){return a[5]-b[5]});
    items_list[i].unshift(empty_item.slice());
    if(i != 0){ // if not sword add empty item to end of array
        items_list[i].push(empty_item.slice());
    }
}

var ring2=ring1;

var res_col=0;
var lvl=0;
var min_lvl=0;
var oa=0;
var summ_oa=0;
var imagesPathPrefix = 'http://'+location.hostname+'/imgs/hwm_items_calcOA/';
function GI(id){
    return document.getElementById(id);
}
function backUp_base(){
    ring1_base=ring1;
    ring2_base=ring2;
    necklace_base=necklace;
    shield_base=shield;
    armor_base=armor;
    shoes_base=shoes;
    helmet_base=helmet;
    back_base=back;
    sword_base=sword;
    modifay_base();
}
function modifay_base(){
    var min_lim=parseInt(GI('min_level').value);
    var max_lim=parseInt(GI('level').value);
    var r_oa=parseInt(GI('search_OA').value);

    for (mod in modification)
    {
        if (modification[mod]){

            if(mod=='ring1')		{ring1=new Array(empty_item.slice(),eval(mod+'_base['+eval("modification['"+mod+"']")+']'));};
            if(mod=='ring2')		{ring2=new Array(empty_item.slice(),eval(mod+'_base['+eval("modification['"+mod+"']")+']'));};
            if(mod=='necklace')	{necklace=new Array(empty_item.slice(),eval(mod+'_base['+eval("modification['"+mod+"']")+']'));};
            if(mod=='shield')	{shield=new Array(empty_item.slice(),eval(mod+'_base['+eval("modification['"+mod+"']")+']'));};
            if(mod=='armor')	{armor=new Array(empty_item.slice(),eval(mod+'_base['+eval("modification['"+mod+"']")+']'));};
            if(mod=='shoes')	{shoes=new Array(empty_item.slice(),eval(mod+'_base['+eval("modification['"+mod+"']")+']'));};
            if(mod=='helmet')	{helmet=new Array(empty_item.slice(),eval(mod+'_base['+eval("modification['"+mod+"']")+']'));};
            if(mod=='back')		{back=new Array(empty_item.slice(),eval(mod+'_base['+eval("modification['"+mod+"']")+']'));};
            if(mod=='sword')	{sword=new Array(empty_item.slice(),eval(mod+'_base['+eval("modification['"+mod+"']")+']'));};
        } else

        {
            var work_array=new Array();
            var q=eval(mod);
            for(gg=0;gg<q.length-1;gg++)
            {
                if(q[gg][2]==0||(q[gg][6]>=min_lim&&q[gg][6]<=max_lim&&q[gg][26])){work_array.push(q[gg]);};
            }
            if(mod!='sword'){work_array.push(q[q.length-1]);}

            eval(mod+'=work_array');
        }
    }
}
function restore_base(){
    ring1=ring1_base;
    ring2=ring2_base;
    necklace=necklace_base;
    shield=shield_base;
    armor=armor_base;
    shoes=shoes_base;
    helmet=helmet_base;
    back=back_base;
    sword=sword_base;
}
function change_Array(item,i,q){
    (q?eval(item+'['+i+'][26]=1;'):eval(item+'['+i+'][26]=0;'));
    GI('rollback').disabled=false;
    GI('rollback').className='MN';
    return true;
}
function rollback(){
    for (i=0;i<items.length;i++)
    {var q=eval(items[i]);
        for(w in q)
        {q[w][26]=1;
        }

    }

    GI('rollback').disabled=true;
    GI('rollback').className='mn_dis';
}

// sword, back, helmet, shoes, armor, shield, necklace, ring1, ring2
//slotList = ['sword', 'necklace', 'shield', 'armor', 'shoes', 'helmet', 'back', 'ring1', 'ring2'];
var slotList = [
    'sword', 'back', 'helmet', 'shoes', 'armor', 'shield', 'necklace', 'ring1', 'ring2'
];

function slotType(name){
    if(!slotType.prototype.byName){
        slotType.prototype.byName = [];
    }
    slotType.prototype.byName[name] = this;
    if(!slotType.prototype.byIndex){
        slotType.prototype.byIndex = [];
    }
    if(!slotType.prototype.count){
        slotType.prototype.count = 0;
    }
    var index = slotType.prototype.count;
    slotType.prototype.count++;
    slotType.prototype.byIndex[index] = this;
    this.name = name;
    this.index = index;
}
for(i in slotList){
    new slotType(slotList[i]);
}
$(function(){
    locdata.load();
});

function calcOA2(){

    if(parseInt(GI('search_OA').value) >= parseInt(GI('selected_oa').value) && parseInt(GI('max_cost').value) >= parseInt(GI('selected_summ').value))
    {
        locdata.save();
        lvl=parseInt(GI('level').value);
        min_lvl=parseInt(GI('min_level').value);
        oa=parseInt(GI('search_OA').value);
        backUp_base();
        var beg_d = new Date();
        var begining=beg_d.getTime();
        var ending=0;
        var mid=0;

        inventory_mask = [];
//    ring1
//    ring2
//    necklace
//    shield
//    armor
//    shoes
//    helmet
//    back
//    sword
        var items = [];
        items['sword'] = sword.slice();
        items['ring1'] = ring1.slice();
        items['ring2'] = ring2.slice();
        items['necklace'] = necklace.slice();
        items['shield'] = shield.slice();
        items['armor'] = armor.slice();
        items['shoes'] = shoes.slice();
        items['helmet'] = helmet.slice();
        items['back'] = back.slice();
        global_items = items;
        inventory_index = [];
        // фильтрация слотов
        for(i in slotList){
            slotName = slotList[i];
            var slotIndex = slotType.prototype.byName[slotName].index;
            if(GI(slotName).checked){
                inventory_mask[slotIndex] = items[slotName];
                if(inventory_mask[slotIndex].length > 1
                    && inventory_mask[slotIndex][inventory_mask[slotIndex].length-1][5] == 0){
                    // убираем последний пустой элемент
                    inventory_mask[slotIndex].pop();
                }
                if(slotName == 'sword'
                    && inventory_mask[slotIndex][inventory_mask[slotIndex].length-1][5] == 0){
                    inventory_mask[slotIndex].unshift();
                }
//            inventory_mask[slotIndex].sort(function(a,b){return a[5] - b[5]});
                inventory_index.push(slotIndex);
            }
        }
        global_maxPossibleOA = [];
        var prevMaxSlotOA = 0;
        for(var i=0; i<inventory_index.length; i++){
            var slotIndex = inventory_index[inventory_index.length-i-1];
            var maxSlotOA = 0;
            for(var j=1; j<inventory_mask[slotIndex].length; j++){
                var itemOA = inventory_mask[slotIndex][j][5];
                maxSlotOA = Math.max(maxSlotOA, itemOA);
            }
            maxSlotOA = maxSlotOA+prevMaxSlotOA;
            global_maxPossibleOA.unshift(maxSlotOA);
            prevMaxSlotOA = maxSlotOA;
        }
        maxCost=parseFloat(GI('max_cost').value);
        global_maxCostDynamic = maxCost;
        if(maxCost==0)maxCost=res_oa*9000;
        needed_OA = parseInt(GI('search_OA').value);
        var max_result=parseInt(GI('max_result').value);
        global_maxResult = Math.max(1, max_result);
        var items_set = [];
        for(i = 0; i<slotType.prototype.count; i++){
            //    var slotName = slotType.prototype.byIndex[i].name;
            //    items_set.push(items[slotName].length-1);
            items_set.push(0);
        }
        calcRecCount = 0;
        calcLoopCount = 0;
        nArr=new Array();
        global_setsCount = 0;
        calcRec(0, 0, 0, items_set);
        console.log('recCount=' + calcRecCount + ' loopCount=' + calcLoopCount
        + ' global_setsCount='+global_setsCount);
        //is_valid('back',w,1);


        /*сортировка полученного массива*/
        nArr.sort(function(a,b){return a[1] - b[1]});

        var mid_d = new Date();
        mid=mid_d.getTime();

        /*отрисовка результата*/
        drawArray();

        res_col=nArr.length;
//    nArr=new Array();
        summ_oa=0;

        restore_base();
        var end_d = new Date();
        ending=end_d.getTime();
        runningTime(ending-mid,2);
        runningTime(mid-begining,1);

    }else{
        alert('Введенные данные противоречивы<br>Попробуйте еще раз');
    }
}
function calcRec(num_slot, cur_cost, cur_OA, items_set){
    var slotIndex = inventory_index[num_slot];
    calcRecCount++;
    for(var i=0; i<inventory_mask[slotIndex].length; i++){
        calcLoopCount++;
        var itemOA = inventory_mask[slotIndex][i][5];
        var nextOA = itemOA+cur_OA;
        items_set[slotIndex] = i;
        if(nextOA < needed_OA){
            if(num_slot+1 < inventory_index.length){
                var nextCost = cur_cost + inventory_mask[slotIndex][i][3];
                if(nextCost < global_maxCostDynamic){
                    if(global_maxPossibleOA[num_slot+1] >= needed_OA-nextOA){
                        calcRec(num_slot+1, nextCost, nextOA, items_set);
                    }
                }
            }
        }else if(nextOA == needed_OA){
            var nextCost = cur_cost + inventory_mask[slotIndex][i][3];
            if(nextCost < global_maxCostDynamic){
                myCreateArray(items_set);
            }
        }else{
//            i = inventory_mask[slotIndex].length-2;
            break;
        }
    }
    items_set[slotIndex] = 0;
}

function check_data(){
    var leftOA=GI('selected_oa');
    var leftOA_v=parseInt(leftOA.value);
    var rightOA=GI('search_OA');
    var rightOA_v=parseInt(rightOA.value);
    var leftsumm=GI('selected_summ');
    var leftsumm_v=parseFloat(leftsumm.value);
    var rightsumm=GI('max_cost');
    var rightsumm_v=parseFloat(rightsumm.value);
    if(leftsumm_v>rightsumm_v){rightsumm.className='MN dis';}else{rightsumm.className='MN';}
    if((leftOA_v==rightOA_v&&!modification['sword'])|| leftOA_v>rightOA_v){rightOA.className='MN dis';}else{rightOA.className='MN';}
    if(leftsumm_v<0)leftsumm.value=0;
    locdata.save();
}
var locdata = {
    params: ['min_level','level', 'max_cost', 'search_OA'],
    clear: function (){
        localStorage.clear();
    },
    save: function(){
        var elm, param;
        for(var i in this.params){
            param = this.params[i];
            elm = GI(param);
            localStorage.setItem(param, elm.value);
        }
    },
    load: function (){
        var elm, param;
        for(var i in this.params){
            param = this.params[i];
            elm = GI(param);
            var value = localStorage.getItem(param);
            if(value){
                elm.value = value;
            }
        }
    },
    show: function (){
        var key;
        for(var i=0; i<localStorage.length; i++){
            key = localStorage.key(i);
            console.log(key+': '+localStorage.getItem(key));
        }
    }
};
function is_valid(item,item_n,valid_type){

    item=eval(item+'['+item_n+']');

    if(valid_type==1)
    {
        return ( ( (item[6]<=lvl&&item[6]>=min_lvl/*арт в диапазоне заданых уровневых интервалов*/) || item[6]==0/*или арт снят*/) && (item[5]+summ_oa)<oa /*оа арта недостаточно для дозавершения комплекта*/)
    } else
    if(valid_type==2)
    {
        return ( ( (item[6]<=lvl&&item[6]>=min_lvl) || item[6]==0) && (item[5]+summ_oa)==oa )
    }

}
function change_sel(n){
    var old=eval(n+'[selected_arts[\''+n+'_s\']]');
    if(GI(n).checked==true){
        GI(n).nextSibling.nextSibling.src='/source/css/hwm_calcOA.html_files/sel.gif';
        GI('selected_oa').value=parseInt(GI('selected_oa').value)+old[5];
        GI('selected_summ').value=parseInt(GI('selected_summ').value)+old[3];

        if(eval('selected_arts[\''+n+'_s\']'))GI(n+'t').style.backgroundImage = 'url("'+imagesPathPrefix+old[1]+'")';
        modification[n]='';
    }else{
        GI('selected_oa').value=parseInt(GI('selected_oa').value)-old[5];
        GI('selected_summ').value=parseInt(GI('selected_summ').value)-old[3];
        GI(n).nextSibling.nextSibling.src='/source/css/hwm_calcOA.html_files/sel_d.gif';
        GI(n+'t').style.backgroundImage = 'url("/source/css/hwm_calcOA.html_files/transparent.gif")';
        GI('items_select').style.display='none';
        modification[n]=0;
    };

    check_data();
}
function close_select(){
    GI('items_select').style.display='none';
    GI('bgOver').style.display='none';
    return false;
}
function gen_select(item, _this)  {
    var min_lim=parseInt(GI('min_level').value);
    var max_lim=parseInt(GI('level').value);
    var n = _this.src;
    if(n.match('sel.gif')){
        var b=eval(item);
        var r=eval('selected_arts.'+item+'_s');

        div_inner='<span style="float:right; cursor: pointer;" onClick="close_select();" name="arts_select" id="arts_select"><b>X</b></span><form name=items_to_show id=items_to_show><div><input type=checkbox checked onclick="chun(this)"><input type=radio name=main value=0 '+(r?'':'checked')+' onclick="change_bg(\''+item+'\',0)">Все арты</div><hr>';
        for(var i=1;i<b.length;i++){
            if(b[i][2]){
                var in_lim=(b[i][6]>=min_lim&&b[i][6]<=max_lim?true:false);
                var is_checked=(b[i][26]?true:false);
                div_inner+='<div'+(in_lim==true?' ':' class="not_in_lim"')+'><input type=checkbox '+(is_checked?' checked ':' ')+' '+((!in_lim||r!=0)?' DISABLED ':'')+'  name="'+item+'_'+i+'" onClick="javascript:change_Array(\''+item+'\','+i+',this.checked);"><input type=radio name=main '+(r==i?' checked ':'')+'value='+i+' onclick="change_bg(\''+item+'\','+i+');check_data();">'+b[i][0]+'</div>';
            }
        }
        div_inner+='</form>'

        var div=GI('items_select');
        div.style.left = (_this.offsetLeft-10) + "px";
        div.style.top  = (_this.offsetTop+11) + "px";
        div.innerHTML=div_inner;
        resol();
        div.style.display='';
    } else {return false;}
}
function resol(){
    var overlay=GI('bgOver');
    overlay.style.height=((document.body.scrollHeight > document.body.offsetHeight)?document.body.scrollHeight:document.body.offsetHeight)-20;
    overlay.style.width=((document.body.scrollWidth > document.body.offsetWidth)?document.body.scrollWidth:document.body.offsetWidth)-23;
    overlay.style.display='block';
}
function chun(t){
    var state=false;
    if(t.checked)state=true;

    for(i=1;i<GI('items_to_show').length;i++) {
        var fch=GI('items_to_show')[i];

        if (fch.type=='checkbox'&&fch.checked!=state) {
            var dbl=false;
            if(fch.disabled==true){fch.disabled=false;dbl=true;}
            fch.checked=state;
            change_Array(fch.name.split('_')[0],fch.name.split('_')[1],state);
            if(dbl){fch.disabled=true;};
        }
    }
}
function change_bg(item_type,i){
    var tch=eval(item_type);
    var old=eval(item_type+'[selected_arts[\''+item_type+'_s\']]');
    GI('selected_oa').value=parseInt(GI('selected_oa').value)-old[5]+tch[i][5];
    GI('selected_summ').value=parseInt(parseFloat(GI('selected_summ').value)*100-old[3]*100+tch[i][3]*100)/100;

    var newImage;
    if(tch[i][2]!=0) {newImage="url('"+imagesPathPrefix+tch[i][1]+"')";}else{newImage="";};


    GI(item_type+'t').style.backgroundImage = newImage;
    GI(item_type+'t').style.backgroundPosition = "center center";
    GI(item_type+'t').style.backgroundRepeat = "no-repeat";
    eval('selected_arts["'+item_type+'_s"]='+i);

    eval('modification["'+item_type+'"]='+i);

    if(tch[i][2]!=0)
    {

        if(tch[i][6]<parseInt(GI('min_level').value))GI('min_level').value=tch[i][6];
        if(tch[i][6]>parseInt(GI('level').value))GI('level').value=tch[i][6];
    }
    close_select();
    check_data();
}

function myCreateArray(items_set){
    global_setsCount++
    q=items_set[0];w=items_set[1];e=items_set[2];
    r=items_set[3];t=items_set[4];y=items_set[5];
    u=items_set[6];o=items_set[7];p=items_set[8];
    if((modification['sword']&&sword_base[modification['sword']]!=sword[q])||(modification['back']&&back_base[modification['back']]!=back[w])||(modification['helmet']&&helmet_base[modification['helmet']]!=helmet[e])||(modification['shoes']&&shoes_base[modification['shoes']]!=shoes[r])||(modification['armor']&&armor_base[modification['armor']]!=armor[t])||(modification['shield']&&shield_base[modification['shield']]!=shield[y])||(modification['necklace']&&necklace_base[modification['necklace']]!=necklace[u])||(modification['ring1']&&ring1_base[modification['ring1']]!=ring1[o])||(modification['ring2']&&ring2_base[modification['ring2']]!=ring2[p])){
        return true;
    }else{
        var res_sum=0;
        var tm_lims=true;
        var res_oa=0;
        var res_sum=0;

        for(i in items_set){
            var slotName = slotType.prototype.byIndex[i].name;
            if(global_items[slotName][items_set[i]][5]){
                res_oa += global_items[slotName][items_set[i]][5];
                res_sum += global_items[slotName][items_set[i]][3];
            };
        }
//        if(sword[q][5]){res_oa+=sword[q][5];res_sum+=sword[q][3];};

        nArr.push(new Array(items_set.slice(),res_sum,res_oa));
        nArr.sort(function(a,b){return a[1]-b[1];});
        if(nArr.length > global_maxResult){
            nArr.pop();
            global_maxCostDynamic = nArr[nArr.length-1][1];
        }
    }
}

function htmlByItem(items_set){
    q=items_set[0];w=items_set[1];e=items_set[2];
    r=items_set[3];t=items_set[4];y=items_set[5];
    u=items_set[6];o=items_set[7];p=items_set[8];
    //console.log(items_set.join('_'));

    var res_text='';
    if((modification['sword']&&sword_base[modification['sword']]!=sword[q])||(modification['back']&&back_base[modification['back']]!=back[w])||(modification['helmet']&&helmet_base[modification['helmet']]!=helmet[e])||(modification['shoes']&&shoes_base[modification['shoes']]!=shoes[r])||(modification['armor']&&armor_base[modification['armor']]!=armor[t])||(modification['shield']&&shield_base[modification['shield']]!=shield[y])||(modification['necklace']&&necklace_base[modification['necklace']]!=necklace[u])||(modification['ring1']&&ring1_base[modification['ring1']]!=ring1[o])||(modification['ring2']&&ring2_base[modification['ring2']]!=ring2[p])){
        return true;
    }else{
        for(i in items_set){
            var slotName = slotType.prototype.byIndex[i].name;
            if(global_items[slotName][items_set[i]][5]){
                res_text += eachItem(global_items[slotName][items_set[i]]);
            };
        }
//        if(sword[q][5]){res_oa+=sword[q][5];res_sum+=sword[q][3];res_text+=eachItem(sword[q]);};
    }
    return res_text;
}

function eachItem(item){
    var fin_item='<b><a href="http://www.heroeswm.ru/art_info.php?id=';
    fin_item+=item[7]+'" target="_blank" title="header=[ '+item[0]+'] body=[<b>Стоимость за бой :</b> '+item[3]+'<br><b>Требуемый уровень:</b>  ';
    fin_item+=item[6]+'<br><b>Прочность:</b>  '+item[4]+'<br><b>Очки амуниции:</b>  '+item[5]+'<br><b>Статы и модификаторы:</b><ul> ';
    for(h=8;h<=25;h++){
        if(item[h]!=0){
            if(h==8)fin_item+='<li><b>Нападение:</b> 	 '+item[h]+'</li>';
            if(h==9)fin_item+='<li><b>Защита:</b> 	 '+item[h]+'</li>';
            if(h==10)fin_item+='<li><b>Сила магии:</b> 	 '+item[h]+'</li>';
            if(h==11)fin_item+='<li><b>Знания:</b> 	 '+item[h]+'</li>';
            if(h==12)fin_item+='<li><b>Инициатива:</b> 	 '+item[h]+'%'+'</li>';
            if(h==13)fin_item+='<li><b>Удача:</b> 	 '+item[h]+'</li>';
            if(h==14)fin_item+='<li><b>Боевой дух:</b> 	 '+item[h]+'</li>';
            if(h==15)fin_item+='<li><b>Сопротивление магии:</b> 	 '+item[h]+'%'+'</li>';
            if(h==16)fin_item+='<li><b>Сопротивление магии огня:</b> 	 '+item[h]+'%'+'</li>';
            if(h==17)fin_item+='<li><b>Сопротивление магии земли:</b> 	 '+item[h]+'%'+'</li>';
            if(h==18)fin_item+='<li><b>Сопротивление магии воздуха:</b> 	 '+item[h]+'%'+'</li>';
            if(h==19)fin_item+='<li><b>Сопротивление магии воды:</b> 	 '+item[h]+'%'+'</li>';
            if(h==20)fin_item+='<li><b>Доп. урон в ближнем бою:</b> 	 '+item[h]+'%'+'</li>';
            if(h==21)fin_item+='<li><b>Защита ближн. урона:</b> 	 '+item[h]+'%'+'</li>';
            if(h==22)fin_item+='<li><b>Защита стр. урона:</b> 	 '+item[h]+'%'+'</li>';
            if(h==23)fin_item+='<li><b>Доп. урон стрелков:</b> 	 '+item[h]+'%'+'</li>';
            if(h==24)fin_item+='<li><b>Пробивание магической защиты:</b> 	 '+item[h]+'%'+'</li>';
            if(h==25)fin_item+='<li><b>Инициатива герою:</b> 	 '+item[h]+'%'+'</li>';
        }
    }

    fin_item+='</ul><br>] delay=[0] fade=[on]"><img src="'+imagesPathPrefix+item[1]+'" alt="['+item[0]+']" width="50" height="50" border=0 art_id='+item[27]+'></a>&nbsp;';
    return fin_item;
}
function drawArray(){
    var max_result=parseInt(GI('max_result').value);
    max_result=((nArr.length)<max_result?(nArr.length):max_result);
    var rrr='';
    if(!nArr.length)
    {rrr+='<table class=cTbl width="100%" border=1><tr><th class=cTh style="padding-left : 0px !important; padding-right : 0px !important;"><br>По введенным параметрам ни одного набора подобрать не удалось.<br>Попробуйте еще раз<br><br></th></tr>';}
    else{rrr+='<table class=cTbl width="100%" border=1 style="padding-left : 0px !important; padding-right : 0px !important;"><tr><th class=cTh style="padding-left : 0px !important; padding-right : 0px !important;">Наборы</th><th class=cTh style="padding-left : 0px !important; padding-right : 0px !important;">стоимость</th><th class=cTh style="padding-left : 0px !important; padding-right : 0px !important;">OA</th></tr>';
        for(g=0;g<max_result;g++){rrr+='<tr><td  align="left">'+htmlByItem(nArr[g][0])+'</td><td  width="100px" align="center"><img  align="absmiddle" src="images/res/b/gold.gif"  alt="[Золото]"><b>'+parseInt(100*nArr[g][1])/100+'</b></td><td width="35px" align="center"><b>'+nArr[g][2]+'</b></td></tr>';}
        rrr+='</table>';}
    GI('resss').innerHTML=rrr;
}
function check(n){
    var m=GI('level');
    var k=GI('min_level');
    var m_v=m.value;
    var k_v=k.value;
    var to_change=0;
    locdata.save();
}
function runningTime(a,b){
    var sec=0;
    var min=0;
    var hour=0;
    if(Math.floor(a/3600000)>0){hour=Math.floor(a/3600000);a-=hours*3600000;}
    if(Math.floor(a/60000)>0){min=Math.floor(a/60000);a-=min*60000;}
    if(Math.floor(a/1000)>0){sec=Math.floor(a/1000);a-=sec*1000;}

    GI(b==1?'r_time':'drow_time').innerHTML=(b==1?'Подбор длился ':'Отрисовка длилась ')+(hour?hour+' часов ':'')+(min?min+' минут ':'')+(sec?sec+' секунд ':'')+a+' милисекунд';
    GI('total').innerHTML='Всего вариантов: '+res_col;
}
