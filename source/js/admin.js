$(function(){
    itemsList.init();
    message.init();
});
var itemsList = {
    init: function(){
        $('.js-item-remove').click(function(e){
            e.preventDefault();
            var id = e.currentTarget.getAttribute('data-id');
            var removeIt = confirm('Уверены что хотите удалить объект № '+id);
            if(removeIt){
                var data = {id: id};
                $.post(e.currentTarget.href, data).done(function(d){
                    if(d == 'true'){
                        message.show('Объект № '+id+' успешно удалён');
                    } else {
                        message.show('Не удалось удалить');
                    }
                }).fail(function(d){
                    message.show('Не удалось удалить');
                });
            }
        });
    }
};
var message = {
    init: function(){

    },
    show: function(message){
        console.log(message);
    }
};