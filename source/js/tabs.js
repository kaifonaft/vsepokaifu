var tabs = {
    init: function(){
        var links = document.querySelectorAll(".js-link");
        var tabswrap = document.querySelector('.js-tabswrap')
//        var tabs = tabswrap.querySelectorAll('.js-tab');
        for(var i in links){
            links[i].onclick = tabs.clickLink;
        }
    },
    clickLink: function(e){
        e.preventDefault();
        var link = e.currentTarget;
        var tabnum = link.getAttribute('data-tabnum');
        var tabswrap = link.closest('.js-tabswrap');
        tabs.setActive(tabswrap, link);
        var tabList = tabswrap.querySelectorAll('.js-tab');
        for(var i=0; i<tabList.length; i++){
            if(i == tabnum-1)
                tabList[i].style.display = 'block';
            else
                tabList[i].style.display = 'none';
        }
    },
    setActive: function(wrap, currentLink){
        var links = wrap.querySelectorAll('.js-link');
        console.log(links);
        for(var i=0; i<links.length; i++){
            var link = links[i];
            //console.log(link, 'linki');
            //console.log(link.classList, 'link classList');
            links[i].classList.remove('active');
        }
        currentLink.classList.add('active');
    }
};
tabs.init();
