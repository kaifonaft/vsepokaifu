var hwmmap = {
    $areas: null,
    $searchItems: null,
    $mapMerchant: null,
    searchTimeoutId: null,
    init: function(){
        var self = this;
        self.$mapMerchant = $('.js-merchant');
        $('.js-map-filter').click(function(e){
            var $button = $(e.currentTarget);
            var regionsStr = $button.data('regions')+'';
            var regions = regionsStr    .split(',');
            self.$mapMerchant.toggleClass('selected');
            self.showRegions(regions);
        });
        self.$mapMerchant.click(function(e){
            self.$mapMerchant.toggleClass('selected');
        });
        self.$areas = $('.js-map-wrap area');
        $("img[usemap]").mapster({
            scaleMap: true,
            fill: true,
            fillColor: '2CA7DC',
            fillOpacity: 0.75
        });
        self.$searchItems = $('.js-search-items');
        self.$searchItems.keyup(function(e){
            if(self.searchTimeoutId){
                clearTimeout(self.searchTimeoutId);
                self.searchTimeoutId = null;
            }
            self.searchTimeoutId = setTimeout(self.filterItems, 300);
        });
    },
    /**
     * @var array regions */
    showRegions: function(regions){
        var self = this;
        for(i in regions){
            var $needArea = self.$areas.filter('area[data-id="'+regions[i]+'"]');
            $needArea.click();
        }
    },
    filterItems: function(){
        var $input = hwmmap.$searchItems;
        var text = $input.val();
        text = text.toLocaleLowerCase();
        var $mapButtons = $('.js-map-filter');
        for(var i=0; i<$mapButtons.length; i++){
            var name = $mapButtons[i].getAttribute('data-name').toLocaleLowerCase();
            if(name.indexOf(text) != -1){
                $mapButtons[i].style.opacity = 1;
            }else{
                $mapButtons[i].style.opacity = 0.5;
            }
        }
    }
};
var sortBy = {
    direction: 'sort_direction',
    param: 'sort_param',
    init: function(){
        $('.js-sort-by').click(function(e){
            e.preventDefault();
            var target = e.currentTarget;
            var newParam = target.getAttribute('data-param'),
                newDir = 'ASC',
                curDir = localStorage.getItem(sortBy.direction),
                curParam = localStorage.getItem(sortBy.param);
            if(curParam == newParam && curDir != 'DESC'){
                newDir = 'DESC';
            }
            localStorage.setItem(sortBy.direction, newDir);
            localStorage.setItem(sortBy.param, newParam);
            sortBy.apply();
        });
        sortBy.apply();
    }
    ,
    apply: function(){
        var curDir = localStorage.getItem(sortBy.direction),
            curParam = localStorage.getItem(sortBy.param),
            dirMulty = (curDir == 'DESC') ? -1 : 1;
        var $filterButtons = $('.js-map-filter');
        $filterButtons.sort(function(a, b){
            var paramAttr = 'data-'+curParam;
            var aParam = a.getAttribute(paramAttr),
                bParam = b.getAttribute(paramAttr);
            if(aParam > bParam){
                return 1 * dirMulty;
            }
            if(aParam < bParam){
                return -1 * dirMulty;
            }
            return 0;
        });
        $filterButtons.detach().appendTo($('.js-legend-buttons'));
    }
};
$(function(){
    hwmmap.init();
    sortBy.init();
});