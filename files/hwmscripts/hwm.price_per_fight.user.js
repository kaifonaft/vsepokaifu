// ==UserScript==
// @name        hwm Price per fight
// @namespace   http://www.heroeswm.ru/auction.php
// @description show price for one fight for item
// @include     http://www.heroeswm.ru/auction.php*
// @require     http://yastatic.net/jquery/2.1.4/jquery.min.js
// @version     0.1
// @grant       none
// ==/UserScript==
//точность
var precision = 2;

// получаем список строк товаров
$items = $('tr.wb');

// меняем заголовок
header = $items[0].previousElementSibling;
priceHeader = header.childNodes[2];
priceHeader.textContent = priceHeader.textContent + ' / ' + 'Цена за бой';

for(var i=0; i<$items.length; i++){
// цикл по всем товарам на странице
    item = $items[i];
    imgs = item.getElementsByTagName('img');
    imgMoneyEl = false;
    for(var j=0; j<imgs.length; j++){
       if(imgs[j].src == 'http://dcdn3.heroeswm.ru/i/gold.gif'){
         imgMoneyEl = imgs[j]; 
         break;
       }
    }
    if(!imgMoneyEl){
        continue;
    }
    priceEl = imgMoneyEl.parentNode.nextElementSibling;
    price = priceEl.textContent.replace(',','');
    matches = item.textContent.match(/Прочность:\s*(\d+)\s*\//);
    proch = matches[1];
    price = parseInt(price);
    pricePerFight = price/proch;
//     указываем цену за бой рядом с ценой
    priceEl.innerHTML = priceEl.textContent + ' / ' + pricePerFight.toFixed(precision);
}
