// ==UserScript==
// @name        noroulete
// @namespace   http://www.heroeswm.ru/
// @description убирает меню рулетка
// @include     http://www.heroeswm.ru/*
// @version     0.3
// @grant       none
// ==/UserScript==

if(window.addEventListener){
  window.addEventListener('load', removeRouleteMenu);
}else{
  window.attachEvent('onload', removeRouleteMenu);
}

function removeRouleteMenu(){
  href = document.querySelector('a[href^=roulette]');
  rouleteMenu = href.closest('td[valign^=middle]').parentNode
   .closest('td[valign^=middle]')
   .parentNode.closest('td[valign^=middle]');
  rouleteMenuBrackets = rouleteMenu.nextSibling;

  rouleteMenu.style.display = 'none';
  rouleteMenuBrackets.style.display = 'none';  
}

if(location.pathname=="/map.php"){
   
  roulRef = document.querySelectorAll('a[href^=roulette]')[1];
  if(roulRef.parentNode.textContent.indexOf('Во время пути Вам доступны') != -1){
    roulRef.textContent = 'Анекдоты :)';
    roulRef.href = 'http://www.anekdot.ru/';
  }
}else if(location.pathname=="/roulette.php"){
  location.href = '/home.php';
}