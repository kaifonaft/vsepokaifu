<?php

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11.07.2017
 * Time: 9:41
 */
class HWMItems_Model extends CI_Model
{
    function __construct(){
        parent::__construct();
    }

    public function remove($id){
        return $this->db->delete('hwm_items', array('id' =>$id));
    }
}