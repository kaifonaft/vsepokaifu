<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.03.2018
 * Time: 17:26
 */

class MY_URI extends CI_URI{
    /**
     * Set URI String
     *
     * @param 	string	$str
     * @return	void
     */
    protected function _set_uri_string($str)
    {
        if(preg_match('/^hwm\./', $_SERVER['SERVER_NAME']))
            $str = 'hwm/'.$str;
        parent::_set_uri_string($str);
    }

    public function filter_uri(&$str)
    {
        $matches = array();
        if ( ! empty($str) && ! empty($this->_permitted_uri_chars) && ! preg_match('/^['.$this->_permitted_uri_chars.']+$/i'.(UTF8_ENABLED ? 'u' : ''), $str, $matches))
        {
            preg_match_all('/[^'.$this->_permitted_uri_chars.']/i'.(UTF8_ENABLED ? 'u' : ''), $str, $matches);
            $matches = array_unique($matches[0]);
            show_error('В URL присутствуют недопустимые символы: '.'('.implode(' ', $matches).')', 400);
        }
    }
}