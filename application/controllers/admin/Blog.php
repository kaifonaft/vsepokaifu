<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once APPPATH.'controllers/CRUDController.php';
class Blog extends CRUDController {
    public function __construct(){
        parent::__construct('blog');
    }
}
