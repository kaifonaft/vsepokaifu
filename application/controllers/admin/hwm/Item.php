<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once APPPATH.'controllers/AdminController.php';
class Item extends AdminController {

    protected $styles;
    public function __construct(){
        parent::__construct();
        $this->styles = array(
            'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
            '/source/css/admin.css',
        );
        $this->load->add_package_path(APPPATH.'third_party/ion_auth/');
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language'));
        $this->load->helper('form');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $form_data = array();
        /** @var CI_DB_active_record $db  */
        $db = $this->db;
        $form_data['items'] = $db->select()
            ->from('hwm_items')
            ->order_by('machine_name', 'ASC')
            ->get()
            ->result_object();
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('hwm_item_list_edit_image', $form_data, true);
        $this->load->view('admin', $data);
    }

    public function editlist()
    {
        $validSort = array('machine_name', 'id', 'name');
        $this->load->library('SortLinks', $validSort, 'id');
        $sortLinks = new SortLinks($validSort, 'id');
        $form_data = array();
        /** @var CI_DB_active_record $db  */
        $db = $this->db;
        if(!empty($_GET['sort']) && in_array($_GET['sort'], $validSort)){
            $sort = $_GET['sort'];
        } else {
            $sort = 'id';
        }
        $validDir = array('asc', 'desc');
        if(!empty($_GET['dir']) && in_array($_GET['dir'], $validDir)){
            $dir = $_GET['dir'];
        } else {
            $dir = 'asc';
        }
        $db->select()
            ->from('hwm_items')
            ->order_by($sort, $dir);
        $form_data['items'] = $db->get()
            ->result_object();
        $sortArrays = [];
        $sortNames = ['id', 'name'];
        $curSort = $sortLinks->getSortOrDefault($sort);
        $curDir = $sortLinks->getDirOrDefault($dir);
        foreach($sortNames as $sortName){
            $link = $sortLinks->getSortLink($curSort, $curDir, $sortName);
            $sortArrays[$sortName]['link'] = '/'.uri_string().(!empty($link) ? '?'.$link : '');

            if($curSort==$sortName){
                if($curDir == 'desc'){
                    $sortArrays[$sortName]['icon'] = 'glyphicon-sort-by-attributes-alt';
                } else {
                    $sortArrays[$sortName]['icon'] = 'glyphicon-sort-by-attributes';
                }
            } else {
                $sortArrays[$sortName]['icon'] = 'glyphicon-sort';
            }
        }

        $form_data['sortArrays'] = $sortArrays;
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('hwm_item_list_edit', $form_data, true);
        $this->load->view('admin', $data);
    }

    public function add(){
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        }
        $this->edit(null, 'add');
    }
    
    public function edit($id, $action = 'edit'){
        include_once 'application/plugins/File.php';
        include_once 'application/plugins/HWM.php';
        $form_data = array();
        /** @var CI_DB_active_record $db  */
        $db = $this->db;
        /** @var CI_DB_result $rdb  */
//        result database for autocomplite
//        $rdb->row_array();
        $this->load->helper('url');
        if(!empty($_POST)){
            $form_data = $_POST;
            if($action == 'edit'){
                $id = intval($_POST['id']);
                $update_data = $form_data;
                unset($update_data['id']);
                $row = $this->db->get_where('hwm_items', array('id' => intval($id)), 1)
                    ->row_array();
                if(!empty($_POST['img']) && $_POST['img'] == 1){
                    File::remove(HWM::itemImgPath.'/'.$row['img']);
                    $update_data['img'] = '';
                }
                if($_FILES['img']['error'] == UPLOAD_ERR_OK){
                    $file_name = $_FILES['img']['name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    if(File::upload($_FILES['img']['tmp_name'], HWM::itemImgPath, $row['machine_name'].'.'.$ext)){
                        $pathTo = rtrim(HWM::itemImgPath,'/').'/'.$row['machine_name'].'.'.$ext;
                        $update_data['img'] = $pathTo;
                    }else{
                        $update_data['img'] = '';
                    }
                }
                $this->db->set($update_data)
                    ->where('id', $id)
                    ->update('hwm_items');
                redirect("/admin/hwm/item/edit/{$id}", 'location', 302);
            }else{
                $this->db->insert('hwm_items', $form_data);

                $id = $this->db->insert_id();
                redirect("/admin/hwm/item/edit/{$id}", 'location', 302);
            }
        }else{
            if($action == 'edit'){
                $form_data = $this->db
                    ->get_where('hwm_items', array('id' => intval($id)), 1)
                    ->row_array();
                if(empty($form_data)){
                    show_404();
                }
            }else{
                $form_data['img'] = '';
            }
        }
        if($action == 'edit'){
            $form_data['submit_button'] = 'Сохранить';
        }else{
            $form_data['submit_button'] = 'Добавить';
        }
        $slot_types = $db->get('hwm_slot')->result_array();
        $form_data['slot_types'] = $slot_types;
        $data['title'] = 'HWM calc OA';
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('forms/hwm_item_edit', $form_data, true);
        $this->load->view('admin', $data);
    }

    public function delete(){
        if(!empty($_POST['id'])){
            $id = intval($_POST['id']);
            $this->load->model('HWMItems_Model');
            $success = $this->HWMItems_Model->remove($id);
        } else {
            $success = false;
        }
        if($success){
            $result = 'true';
        } else {
            $result = 'false';
        }
        echo $result;
    }

    public function csv(){
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        }
        $form_data = array();
        /** @var Messager $messager */
        include_once 'application/plugins/Messager.php';
        include_once 'application/plugins/Csv_parser.php';
        $messager = Messager::getInstance();
        $uploads_dir = rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/files/hwmcsv';
        $columns = array(
            'name',
            'machine_name',
            'durability',
            'price',
            'points_ammunition',
            'min_lvl',
            'description',
            'slot',
        );
        if (!empty($_FILES) && $_FILES['file']['error'] == UPLOAD_ERR_OK) {
            $tmp_name = $_FILES["file"]["tmp_name"];
            $name = $_FILES["file"]["name"];
            $uploadFilePath = "$uploads_dir/$name";
            move_uploaded_file($tmp_name, $uploadFilePath);
            $parser = new Csv_parser(
                'hwm_items',
                $columns,
                'machine_name'
            );
            if($parser->parseFile($uploadFilePath)){
                $messager->addMessage('Файл успешно распарсен');
            }else{
                $messager->addMessage('Ошибка обработки файла');
            }
        }
        $form_data['messages'] = $messager->renderMessages();
        $form_data['columns'] = $columns;
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('forms/hwm_csv', $form_data, true);
        $this->load->view('admin', $data);
    }
}
