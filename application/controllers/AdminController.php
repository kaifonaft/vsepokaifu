<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminController extends CI_Controller {

    protected  $styles;
    public function __construct(){
        parent::__construct();
        if(ENVIRONMENT == 'production')
            $bootstrapLink = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css';
        else
            $bootstrapLink = '/source/css/bootstrap.min.css';
        $this->styles = array(
            $bootstrapLink,
            '/source/css/admin.css',
        );
        $this->load->add_package_path(APPPATH.'third_party/ion_auth/');
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language'));
        $this->load->helper('form');
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $data['styles'] = $this->styles;
        $data['content'] = '';
        $this->load->view('admin', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */