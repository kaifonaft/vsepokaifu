<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once APPPATH.'controllers/AdminController.php';
abstract class CRUDController extends AdminController {
    protected $tableName;
    protected $viewName;

    public function __construct($tableName){
        $this->tableName = $tableName;
        parent::__construct();
    }

    public function index()
    {
        $form_data = array();
        /** @var CI_DB_active_record $db  */
        $db = $this->db;
        $form_data['items'] = $db->select()
            ->from($this->tableName)
            ->get()
            ->result_object();
        $form_data['tableName'] = $this->tableName;
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('forms/itemlist', $form_data, true);
        $this->load->view('admin', $data);
    }

    public function add()
    {
        $this->edit(null, 'add');
    }

	public function edit($id, $action = 'edit')
	{
        $form_data = [];
        if(!empty($_POST)){
            $form_data = $_POST;
            if($action == 'edit'){
                $id = intval($_POST['id']);
                $update_data = $form_data;
                unset($update_data['id']);
                $this->db->set($update_data)
                    ->where('id', $id)
                    ->update($this->tableName);
                redirect("/admin/{$this->tableName}/edit/{$id}", 'location', 302);
            } else {
                $this->db->insert($this->tableName, $form_data);

                $id = $this->db->insert_id();
                redirect("/admin/{$this->tableName}/edit/{$id}", 'location', 302);
            }
        } else {
            if($action == 'edit'){
                $form_data = $this->db
                    ->get_where($this->tableName, array('id' => intval($id)), 1)
                    ->row_array();
                if(empty($form_data)){
                    show_404();
                }
            }
        }
        if($action == 'edit'){
            $form_data['submit_button'] = 'Сохранить';
        }else{
            $form_data['submit_button'] = 'Добавить';
        }

        $data = [];
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('forms/edit', $form_data, true);
        $this->load->view('admin', $data);
	}

    public function delete()
    {
        if(!empty($_POST['id'])){
            $id = intval($_POST['id']);
            $modelName = $this->tableName.'_Model';
            $this->load->model($modelName);
            $success = $this->{$modelName}->remove($id);
        } else {
            $success = false;
        }
        $result = $success ? 'true' : 'false';
        echo $result;
    }
}
