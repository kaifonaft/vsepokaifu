<?php
/**
 * контроллер статических страниц
 *
 * @author Александр
 */
class Pages extends CI_Controller{
    /**
     * выводит статическую страницу
     * @param string $page имя статической страницы
     */
    public function view($page = 'home'){
        $filename = APPPATH.'/views/pages/'.$page.'.php';
        if(!file_exists($filename)){
            show_404();
        }
        $data['title'] = ucfirst($page);

        $data = [];
        $data['content'] = $this->load->view('pages/'.$page, [], true);
        $this->load->view('templates/blog', $data);
    }
}
