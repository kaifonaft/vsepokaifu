<?
class Test extends CI_Controller {

    private $styles;
    public function __construct(){
        parent::__construct();

        $this->styles = array(
            '/source/css/hwm_calcOA.html_files/style.css',
            '/source/css/hwm.css',
        );
            $this->js = array(
            '/source/js/main.js'
        );
    }

    public function index(){
        $this->load->add_package_path(APPPATH.'third_party/ion_auth/');
        $this->load->library(array('ion_auth','form_validation'));
        $a = new Ion_auth_model();
        echo 'hash: '.$a->hash_password('admin');
    }

	public function action1(){
		echo 'action1';
	}
	
	public function action2(){
		echo 'action2';
	}

    public function imgset(){
        /** @var CI_DB_active_record $db  */
        $db = $this->db;
        $items = $db->select()
            ->from('hwm_items')
            ->group_by('hwm_items.id')
            ->get()
            ->result_object();
        foreach($items as $item){
            $newitem = $item;
            $newitem->img = '/imgs/hwm_items/'.$item->machine_name.'.jpg';
            $this->db->set($newitem)
            ->where('id', $item->id)
            ->update('hwm_items');
        }
    }

    public function imgname(){
        if($dir = opendir(rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/imgs/hwm_items')){
            while (($file = readdir($dir)) !== false) {
                echo $file.',';
            }
            closedir($dir);
        }
    }
}
