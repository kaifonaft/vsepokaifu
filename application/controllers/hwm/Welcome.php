<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

    private $styles;
    public function __construct(){
        parent::__construct();

        $this->styles = array(
            '/source/css/hwm_calcOA.html_files/style.css',
            '/source/css/hwm.css',
        );
    }

    public function index(){
        $data = array();
        $data['title'] = 'HWM scripts';
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('pages/hwm_scripts.php', $data, true);
        $this->load->view('templates/hwm', $data);
    }
    
    public function calcOA(){
        $this->load->helper('url');
        $data['title'] = 'HWM calc OA';
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('pages/hwm_calcOA.php', $data, true);
        $this->load->view('templates/hwm', $data);
    }

    public function itemlib(){
        $data['title'] = 'HWM calc OA';
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('pages/hwm_itemlib.php', $data, true);
        $this->load->view('templates/hwm', $data);
    }

    public function map(){
        $this->load->helper('url');
        $data['title'] = 'HWM map';
        $data['styles'] = $this->styles;
        /** @var CI_DB_active_record $db  */
        $db = $this->db;
        /** @var CI_DB_result $rdb  */
        $data['items'] = $db->select(
                array(
                    'hwm_items.*',
                    'GROUP_CONCAT(DISTINCT hwm_regions.id ORDER BY hwm_regions.id SEPARATOR \',\') AS regions',
                )
            )
            ->from('hwm_items')
            ->join('hwm_companies', 'hwm_companies.item_id = hwm_items.id', 'LEFT')
            ->join('hwm_regions', 'hwm_regions.id = hwm_companies.region_id', 'LEFT')
            ->group_by('hwm_items.id')
            ->order_by('slot', 'ASC')
            ->get()
            ->result_object();
        $data['content'] = $this->load->view('pages/hwm_map.php', $data, true);
        $this->load->view('templates/hwm', $data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */