<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mapparse extends CI_Controller {

    private $styles;
    public function __construct(){
        parent::__construct();
        $this->styles = array(
            '/source/css/bootstrap.min.css'
        );
        $this->load->helper('form');
    }

	public function index()
	{
        $data = array();
        if(!empty($_POST) && !empty($_POST['parse'])){
            /** @var CI_DB_active_record $db */
            $db = $this->db;
            $result = json_decode($_POST['parse']);
            if(!empty($result)){
                $companies = $result->companies;

                foreach($companies as $company){
                    $dbData = $company;
                    $dbData->region_id = $result->region_id;
                    $dbRow = $db->get_where('hwm_companies', array('obj_id' => $dbData->obj_id));
                    if(!empty($dbRow) && $dbRow->num_rows > 0){
                        $db->set($dbData)
                            ->where('obj_id', $dbData->obj_id)
                            ->update('hwm_companies');
                    }else{
                        $db->insert('hwm_companies', $dbData);
                    }
                }
            }
        }
        $form_data = array();
        $data['styles'] = $this->styles;
        $form_data['url'] = '/mapparse';
        $data['content'] = $this->load->view('parse', $form_data, true);
        $this->load->view('templates/clear', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */