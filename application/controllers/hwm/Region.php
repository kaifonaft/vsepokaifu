<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Region extends CI_Controller {

    private $styles;
    public function __construct(){
        parent::__construct();
        $this->styles = array(
            '/source/css/hwm_calcOA.html_files/style.css',
//            '/source/css/hwm.css',
            '/source/css/bootstrap.min.css',
            '/source/css/admin.css',
        );
        $this->load->add_package_path(APPPATH.'third_party/ion_auth/');
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language'));
        $this->load->helper('form');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        }
        $data = array();
        $data['styles'] = $this->styles;
        $this->load->view('hwm', $data);
	}
    
    public function add(){
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        }
        $this->edit(null, 'add');
    }
    
    public function edit($id = null, $action = 'edit'){
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        }
        $form_data = array();
        /** @var CI_DB_active_record $a  */
//        $a->get_where();
        /** @var CI_DB_result $b  */
//        $b->row_array();
        $this->load->helper('url');
        if(!empty($_POST)){
            $form_data = $_POST;
            if($action == 'edit'){
                $id = intval($_POST['id']);
                unset($form_data['id']);
                $this->db->set($form_data)
                    ->where('id', $id)
                    ->update('hwm_regions');
                redirect("/hwm/region/edit/{$id}", 'location', 302);
            }else{
                $this->db->insert('hwm_regions', $form_data);

                $id = $this->db->insert_id();
                redirect("/hwm/region/edit/{$id}", 'location', 302);
            }
        }else{
            if($action == 'edit'){
                $form_data = $this->db
                    ->get_where('hwm_regions', array('id' => intval($id)), 1)
                    ->row_array();
                if(empty($form_data)){
                    show_404();
                }
            }
        }
        if($action == 'edit'){
            $form_data['submit_button'] = 'Сохранить';
        }else{
            $form_data['submit_button'] = 'Добавить';
        }
        $data['title'] = 'HWM calc OA';
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('forms/hwm_region_edit', $form_data, true);
        $this->load->view('admin', $data);
    }

    public function csv(){
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        }
        $form_data = array();
        /** @var Messager $messager */
        require_once 'application/plugins/Messager.php';
        require_once 'application/plugins/Csv_parser.php';
        $messager = Messager::getInstance();
        $uploads_dir = rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/files/hwmcsv';
        $columns = array(
            'name', 'machine_name', 'coord_x', 'coord_y'
        );
        if (!empty($_FILES) && $_FILES['file']['error'] == UPLOAD_ERR_OK) {
            $tmp_name = $_FILES["file"]["tmp_name"];
            $name = $_FILES["file"]["name"];
            $uploadFilePath = "$uploads_dir/$name";
            move_uploaded_file($tmp_name, $uploadFilePath);
            $parser = new Csv_parser(
                'hwm_regions',
                $columns,
                'machine_name'
            );
            if($parser->parseFile($uploadFilePath)){
                $messager->addMessage('Файл успешно распарсен');
            }else{
                $messager->addMessage('Ошибка обработки файла');
            }
        }
        $form_data['messages'] = $messager->renderMessages();
        $form_data['columns'] = $columns;
        $data['styles'] = $this->styles;
        $data['content'] = $this->load->view('forms/hwm_csv', $form_data, true);
        $this->load->view('admin', $data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */