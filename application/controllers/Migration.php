<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.02.2016
 * Time: 23:37
 */

class Migration extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!is_cli())
            show_error('No web access. Use command line instead', 403, "Error 403");
        $this->load->library('migration');
    }

    public function index(){
//        echo 'current: '.$this->migration->version().PHP_EOL;
//        echo 'all cool'.PHP_EOL;
    }

    public function latest(){
        echo 'before lib'.PHP_EOL;
        if ($this->migration->latest() === FALSE)
        {
            show_error($this->migration->error_string());
        }
        echo 'all cool'.PHP_EOL;
    }

    public function down(){
        echo 'down'.PHP_EOL;
        if ($this->migration->version($this->migration->current()-1) === FALSE)
        {
            show_error($this->migration->error_string());
        } else {
            echo 'all cool'.PHP_EOL;
        }
    }
}