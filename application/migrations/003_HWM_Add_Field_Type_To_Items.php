<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_HWM_Add_Field_Type_To_Items extends CI_Migration{
    public function up(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->add_column('hwm_items', array(
            'slot' => array(
                'type' => 'INT',
                'null' => TRUE,
            ),
        ));

        $dbforge->add_field('id');
        $dbforge->add_field(array(
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
        ));
        $dbforge->create_table('hwm_slot', true);
    }

    public function down(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->drop_column('hwm_items', 'slot');

        $dbforge->drop_table('hwm_slot');
    }
}