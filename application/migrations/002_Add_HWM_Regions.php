<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_HWM_Regions extends CI_Migration{
    public function up(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->add_field('id');
        $dbforge->add_field(array(
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'machine_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '120',
            ),
            'coord_x' => array(
                'type' => 'INT',
            ),
            'coord_y' => array(
                'type' => 'INT',
            ),
        ));
        $dbforge->create_table('hwm_regions', true);
    }

    public function down(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->drop_table('hwm_regions');
    }
}