<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_HWM_Items extends CI_Migration{
    public function up(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->add_field('id');
        $dbforge->add_field(array(
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'machine_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '120',
            ),
            'durability' => array(
                'type' => 'INT',
            ),
            'price' => array(
                'type' => 'INT',
            ),
            'points_ammunition' => array(
                'type' => 'INT',
                'null' => TRUE,
            ),
            'min_lvl' => array(
                'type' => 'INT',
                'null' => TRUE,
            ),
            'description' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
        ));
        $dbforge->create_table('hwm_items', true);
    }

    public function down(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->drop_table('hwm_items');
    }
}