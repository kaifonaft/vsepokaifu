<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_HWM_Companies extends CI_Migration{
    public function up(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->add_field('id');
        $dbforge->add_field(array(
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'obj_id' => array(
                'type' => 'INT',
            ),
            'item_id' => array(
                'type' => 'INT',
            ),
            'region_id' => array(
                'type' => 'INT',
            ),
        ));
        $dbforge->create_table('hwm_companies', true);
    }

    public function down(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->drop_table('hwm_companies');
    }
}