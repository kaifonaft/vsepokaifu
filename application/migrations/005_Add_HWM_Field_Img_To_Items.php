<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_HWM_Field_Img_To_Items extends CI_Migration{
    public function up(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->add_column('hwm_items',array(
            'img' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
        ));
    }

    public function down(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->drop_column('hwm_items','img');
    }
}