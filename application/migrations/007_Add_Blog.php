<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_Blog extends CI_Migration{
    private $TABLE_NAME = 'blog';
    public function up(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->add_field('id');
        $dbforge->add_field(array(
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'translit' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'content' => array(
                'type' => 'TEXT',
            ),
        ));
        $dbforge->create_table($this->TABLE_NAME, true);
    }

    public function down(){
        /** @var $dbforge CI_DB_forge */
        $dbforge = $this->dbforge;
        $dbforge->drop_table($this->TABLE_NAME);
    }
}