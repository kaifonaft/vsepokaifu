<h1>Игры для 2ух игроков на одном компьютере</h1>
<div class="js-tabswrap">
<ul class="game2players-links">
    <li class="game2players-link-li">
        <a href="#game-1" class="js-link game2players-link active" data-tabnum="1">
            <div class="game2players-name">Mortal Combat 3 Ultimate(kabal)</div>
            <div class="game2players-img-wrap">
                <img src="/imgs/game2player/mk3-mini.jpg" alt="The fly" class="game2players-img"/>
            </div>
        </a>
    </li>
    <li class="game2players-link-li">
        <a href="#game-2" class="js-link game2players-link" data-tabnum="2">
            <div class="game2players-name">The fly</div>
            <div class="game2players-img-wrap">
                <img src="/imgs/game2player/thefly-mini.jpg" alt="The fly" class="game2players-img"/>
            </div>
        </a>
    </li>
    <li class="game2players-link-li">
        <a href="#game-3" class="js-link game2players-link" data-tabnum="3">
            <div class="game2players-name">Boomberman</div>
            <div class="game2players-img-wrap">
                <img src="/imgs/game2player/boomberman-mini.jpg" alt="Boomberman" class="game2players-img"/>
            </div>
        </a>
    </li>
    <li class="game2players-link-li">
        <a href="#game-4" class="js-link game2players-link" data-tabnum="4">
            <div class="game2players-name">Worms Armageddon</div>
            <div class="game2players-img-wrap">
                <img src="/imgs/game2player/worms-mini.jpg" alt="Worms Armageddon" class="game2players-img"/>
            </div>
        </a>
    </li>
    <div class="clear"></div>
</ul>
<div class="clear"></div>
<div class="game2players-content-wrap">
    <div class="game2players-content js-tab">
        <h2>Mortal Combat 3 Ultimate(kabal)</h2>
        Наверное лучший МК из всех. Здесь собраны персонажи с первых 3ех МК. Включая "толстяков" и "шаоканов".<br>
        Сборка от kabal-а на базе MK 3 Ultimate. Запускаяется через sega-gens:<br>
        gens.exe -&gt; Open room -&gt (5125)UMKT(HACK_23)_fixed.bin<br>
        <img src="/imgs/game2player/mk3.jpg" alt="Mortal kombat 3"/>
    </div>
    <div class="game2players-content js-tab">
        <h2>The fly</h2>
        Забавная игра про назойливую муху и фермера.<br>
        Один игрок играет за муху и пытается стырить со стола как можно больше ценного.<br>
        Другой играет за фермера т.е. за руку c мухобойкой. И пытается убить муху как можно большее число раз.<br>
        <img src="/imgs/game2player/thefly.jpg" alt="The fly"/>
    </div>
    <div class="game2players-content js-tab">
        <h2>Boomberman</h2>
        Бумбермеееееееееен!!!!!<br>
        <img src="/imgs/game2player/boomberman.jpg" alt="Boomberman"/>
    </div>
    <div class="game2players-content js-tab">
        <h2>Worms armageddon</h2>
        <p>Червячки.</p>
        <img src="/imgs/game2player/worms.jpg" alt="Boomberman"/>
    </div>
</div>
</div>
Все игры можно скачать <a href="https://yadi.sk/d/XyKpmUtpdtLwT">здесь</a>
<script type="text/javascript" src="/source/js/tabs.js"></script>