<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>CSS-grid новый "display" для верстки</title>
</head>
<body>
<style>
    html, body{
        height: 100%;
    }
    .container {
        display: grid;
        width: 100%;
        height: 100%;
        grid-template-columns: 200px 1fr 1fr;
        grid-template-rows: 80px 1fr 1fr 100px;
        grid-gap: 1rem;
        grid-template-areas:
        "header header header"
        "sidebar content-1 content-1"
        "sidebar content-2 content-3"
        "footer footer footer";
    }
    .block{
        text-align: center;
        font-size: 20px;
        box-sizing: border-box;
        padding-top: 30px;
        border: 1px solid black;
    }
    .block::after{
        content: '';
        cursor: e-resize;
        width: calc(100% + 5px);
        color: red;
    }
    .header {
        grid-area: header;
        background: lightcoral;
    }

    .sidebar {
        grid-area: sidebar;
        background: lightblue;
    }

    .content-1 {
        grid-area: content-1;
    }

    .content-2 {
        grid-area: content-2;
    }

    .content-3 {
        grid-area: content-3;
    }

    .footer {
        grid-area: footer;
        background: brown;
    }

    .grid{
        display: grid;
    }
</style>
<div class="container">
    <div class="block header">header <h1>CSS-grid - новый "display" для вёрстки</h1>
    </div>
    <div class="block sidebar">sidebar</div>
    <div class="block content-1">Content-1</div>
    <div class="block content-2">Content-2</div>
    <div class="block content-3">Content-3</div>
    <div class="block footer">footer</div>
</div>
</body>
</html>
