<link type='text/css' href='/source/css/hwm_calcOA.html_files/style.css?20120206' rel='stylesheet' />
<link type='text/css' href='/source/css/hwm_calcOA.html_files/ui.base.css?20120206' rel='stylesheet' />
<link rel="stylesheet" href="/source/css/calcoa_inline.css" type="text/css"/>

<div id="page_bg" bgcolor="#000000" text="#888888">
    <table style="margin: 0 auto;border: 2px solid black;" width="900" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td class="capmain">Расчет ОА</td>
        </tr>
        <tr>
            <td class="main-body">
                <br><br><b><font color="#ee5533">Вниманиие, при вводе значения ОА 30-50 браузер рискует зависнуть на срок до 4 мин.
                        Рекомендуется вводить ограничения(исключения из переборов, обязательный арт в наборе, лимиты по цене и уровню)</font></b>
                <div class="js-toggle-wrap">
                    <h4 style="cursor:pointer" tgl="makeToggle" class="js-toggle-button"><span> +</span> Копирайты</h4>
                    <span style="display: none" class="js-toggle-div">
                  Вёрстка взята с сайта <a href="http://witchhammer.ru/">http://witchhammer.ru/</a>.<br>
                  Автор оригинального скрипта -  HAPblB.<br>
                  Мною были полностью переписаны алгоритмы перебора и отрисовки. Теперь скрипт работает ЗНАЧИТЕЛЬНО быстрее
                  </span>
                </div>
                <div class="js-toggle-wrap">
                    <h4 style="cursor:pointer" tgl="makeToggle" class="js-toggle-button"><span> +</span> Инструкция по эксплуатации</h4>
                    <span style="display: none" class="js-toggle-div">
                     С момента ввода в игру понятия Мин ОА большинство игроков сталкивается с проблемой - «как набрать необходимое количество ОА, при этом минимизировав свои затраты», также существует ряд других ситуаций (ивенты, различные спец требования для боев) в которых необходимо определенное значение ОА.<br>
                     Для того чтобы подобрать комплект который удовлетворяет требованию ОА и требованию  минимальной стоимости за бой следует воспользоваться данным сервисом.<br><br><br>
                     <h4>Описание интерфейса:</h4>
                     <br>
                     <b>Кукла с «активными» слотами.</b>
                     На каждый слот можно выставить чекбокс (нет чекбокса — запрет на использование артефакта из этого слота в итоговом наборе)<br>
                     Щелкнув «Треугольник» можно выбрать набор артефактов который будет участвовать в формировании набора (можно выбрать обязательный  артефакт для набора — то есть именно он и будет использоваться в итоговых наборах, причем  должен в итоговом наборе находиться обязательно)<br><br>
                     <b>Блок с управляющими элементами</b><br>
                     <li>«Подобрать наборы» - кнопка инициирующая процесс подбора наборов.</li>
                     <li>«Введите желаемый ОА» - сколько ОА должно быть в наборе</li>
                     <li>«Граница стоимости комплекта» - максимальная себестоимость искомого комплекта</li>
                     <li>«Откатить исключения» - в подборе участвуют все подходящие артефакты всех слотов (отменяет ручное выставление артефактов для слотов (через меню «треугольника»)</li>
                     <li>«Набрано ОА» - суммируются ОА всех выбранных вручную артефактов из меню «треугольник» </li>
                     <li>«Набрано на стоимость» - суммируется стоимость всех выбранных вручную артефактов из меню «треугольник» </li>
                     <li>«min уровень артов», «max уровень артов» - артефакты каких уровней участвуют в формировании наборов</li>
                     <li>«Вывести результатов» - количество выводимых подобранных наборов</li>
                     <br><br>
                     <b>Результат</b><br>
                     Сортируется по цене набора по возрастающей.
                     Все цены указаны с округлением до сотых.
                     <b>Примечание:</b><br>
                     На время работы сервиса влияет отрисовка, которая зависит от количества артефактов, необходимых вывести на экран. Регулируется окошком «Вывести результатов»
                  </span>
                </div>
                <br><br>
                <div style="position: absolute; left: 0pt; width: 100%; top: 0px; height: 1450px; background: none repeat scroll 0% 0% rgb(0, 0, 0); opacity: 0.01;  filter: alpha(opacity=1); -moz-opacity: 0.01; display: none; z-Index: 990" onclick="close_select();" id="bgOver"></div>
                <div style="display: none; position: absolute; z-Index: 999;  background: #F5F3EA; border: #222 solid 2px; margin: 2px; color: #592C08; font-size: 12px; padding: 2px; width=200px;" id="items_select"></div>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <th class="cTh" style="PADDING-RIGHT: 0px !important; PADDING-LEFT: 0px !important; PADDING-BOTTOM: 0px !important; PADDING-TOP: 0px !important;" width="170px" align="left">
                            <table style="background-image: url('/source/css/hwm_calcOA.html_files/br/kukla1.jpg');" width="167" height="204" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <td class="hitem" id="ring1t" width="50" height="48">
                                        <input type="checkbox" onclick="change_sel(this.id)" id="ring1" checked="">
                                        <img src="/source/css/hwm_calcOA.html_files/sel.gif" onclick="javascript:gen_select('ring1',this);" width="13" height="13" border="0">
                                    </td>
                                    <td style="BORDER-TOP: #000000 1px solid;" width="4">
                                        <img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="4" height="48" border="0">
                                    </td>
                                    <td class="hitem" id="helmett" width="50" height="48">
                                        <input type="checkbox" onclick="change_sel(this.id)" id="helmet" checked="">
                                        <img src="/source/css/hwm_calcOA.html_files/sel.gif" onclick="javascript:gen_select('helmet',this);" width="13" height="13" border="0">
                                    </td>
                                    <td style="BORDER-TOP: #000000 1px solid;" width="4"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="4" height="48" border="0"></td>
                                    <td class="hitem" id="necklacet" width="50" height="50">
                                        <input type="checkbox" onclick="change_sel(this.id)" id="necklace" checked="">
                                        <img src="/source/css/hwm_calcOA.html_files/sel.gif" onclick="javascript:gen_select('necklace',this);" width="13" height="13" border="0">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="BORDER-LEFT: #000000 1px solid;" width="50"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="50" height="22" border="0"></td>
                                    <td width="6"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="6" height="22" border="0"></td>
                                    <td width="50"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="50" height="22" border="0"></td>
                                    <td width="6"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="6" height="22" border="0"></td>
                                    <td style="BORDER-RIGHT: #000000 1px solid;" width="50"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="50" height="22" border="0"></td>
                                </tr>
                                <tr>
                                    <td class="hitem" id="ring2t" width="50">
                                        <input type="checkbox" onclick="change_sel(this.id)" id="ring2" checked="">
                                        <img src="/source/css/hwm_calcOA.html_files/sel.gif" onclick="javascript:gen_select('ring2',this);" width="13" height="13" border="0">
                                    </td>
                                    <td width="4"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="4" height="48" border="0"></td>
                                    <td class="hitem" id="armort" width="50">
                                        <input type="checkbox" onclick="change_sel(this.id)" id="armor" checked="">
                                        <img src="/source/css/hwm_calcOA.html_files/sel.gif" onclick="javascript:gen_select('armor',this);" width="13" height="13" border="0">
                                    </td>
                                    <td width="4"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="4" height="48" border="0"></td>
                                    <td class="hitem" id="backt" width="50">
                                        <input type="checkbox" onclick="change_sel(this.id)" id="back" checked="">
                                        <img src="/source/css/hwm_calcOA.html_files/sel.gif" onclick="javascript:gen_select('back',this);" width="13" height="13" border="0">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="BORDER-LEFT: #000000 1px solid;" width="50"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="50" height="22" border="0"></td>
                                    <td width="6"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="6" height="22" border="0"></td>
                                    <td width="50"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="50" height="22" border="0"></td>
                                    <td width="6"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="6" height="22" border="0"></td>
                                    <td style="BORDER-RIGHT: #000000 1px solid;" width="50"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="50" height="22" border="0"></td>
                                </tr>
                                <tr>
                                    <td class="hitem" id="swordt" width="50">
                                        <input type="checkbox" onclick="change_sel(this.id)" id="sword" checked="" disabled="">
                                        <img src="/source/css/hwm_calcOA.html_files/sel.gif" onclick="javascript:gen_select('sword',this);" width="13" height="13" border="0">
                                    </td>
                                    <td style="BORDER-BOTTOM: #000000 1px solid;" width="4">
                                        <img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="4" height="48" border="0">
                                    </td>
                                    <td class="hitem" id="shoest" width="50">
                                        <input type="checkbox" onclick="change_sel(this.id)" id="shoes" checked="">
                                        <img src="/source/css/hwm_calcOA.html_files/sel.gif" onclick="javascript:gen_select('shoes',this);" width="13" height="13" border="0">
                                    </td>
                                    <td style="BORDER-BOTTOM: #000000 1px solid;" width="4"><img src="/source/css/hwm_calcOA.html_files/transparent.gif" width="4" height="48" border="0"></td>
                                    <td class="hitem" id="shieldt" width="50">
                                        <input type="checkbox" onclick="change_sel(this.id)" id="shield" checked="">
                                        <img src="/source/css/hwm_calcOA.html_files/sel.gif" onclick="javascript:gen_select('shield',this);" width="13" height="13" border="0">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </th>
                        <td width="3px">&nbsp;</td>
                        <th class="cth" style="TEXT-ALIGN: center !important; VERTICAL-ALIGN: TOP !important;">
                            <table width="514px" border="0">
                                <tbody>
                                <tr>
                                    <td colspan="5" style="vertical-align: top; height:15px;"><span height="15" border="0" style="float:left"><input type="button" class="mn_dis" value="Откатить исключения" id="rollback" onclick="rollback();" disabled=""></span><span height="15" border="0" id="r_time" style="float:right">Подбор длился 28 милисекунд</span></td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="vertical-align: top; height:15px;"><span height="15" border="0" id="drow_time" style="float:right">Отрисовка длилась 31 милисекунд</span></td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="vertical-align: top; height:35px;"><span height="15" border="0" id="total" style="float:right">Всего вариантов: 20</span></td>
                                </tr>
                                <tr>
                                    <td align="right">min уровень артов: </td>
                                    <td>
                                        <select id="min_level" style="text-align: center; width: 50px; visibility: visible;" onchange="check(this)" class="MN">
                                            <option value="1" selected="">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                        </select>
                                    </td>
                                    <td width="20">&nbsp;</td>
                                    <td align="right">Введите желаемый ОА: </td>
                                    <td align="right"><input style="TEXT-ALIGN: center; width:50px; " class="MN" type="text" id="search_OA" onchange="check_data();" onkeyup="check_data();" value="10"></td>
                                </tr>
                                <tr>
                                    <td align="right">max уровень артов: </td>
                                    <td>
                                        <select id="level" style="text-align: center; width: 50px; visibility: visible;" onchange="check(this)" class="MN">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10" selected="">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                        </select>
                                    </td>
                                    <td width="20">&nbsp;</td>
                                    <td align="right">Граница стоимости комплекта: </td>
                                    <td align="right"><input style="TEXT-ALIGN: center; width:50px;" class="MN" type="text" onchange="check_data();" onkeyup="check_data();" id="max_cost" value="140"></td>
                                </tr>
                                <tr>
                                    <td align="right">Вывести результатов: </td>
                                    <td><input style="TEXT-ALIGN: center; width:50px;" class="MN" type="text" id="max_result" value="20"></td>
                                    <td width="20">&nbsp;</td>
                                    <td colspan="2" align="right"><input value="Подобрать наборы" type="button" class="MN" style="TEXT-ALIGN: center; width:100%; height: 20px; VERTICAL-ALIGN: middle; background-image: url('/themes/Skull_my/images/cat2_back.gif') !important; background-position: 0 0; background-repeat: repeat;" onclick="javascript:calcOA2();"></td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <hr>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Набрано ОА: </td>
                                    <td><input style="TEXT-ALIGN: center; width:50px;" class="MN" readonly="" type="text" id="selected_oa" value="0"></td>
                                    <td width="20">&nbsp;</td>
                                    <td align="right">Набрано на стоимость: </td>
                                    <td><input style="TEXT-ALIGN: center; width:50px;" class="MN" readonly="" type="text" id="selected_summ" value="0"></td>
                                </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                    </tbody>
                </table>
                <br>
                <div id="resss"></div>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
                <script type="text/javascript" src="/source/css/hwm_calcOA.html_files/jscript.js"></script>
                <script type="text/javascript" src="/source/css/hwm_calcOA.html_files/calcoa.js"></script>
            </td>
        </tr>
        </tbody>
    </table>
</div>
