<div>Скрипты. Ссылки для быстрой установки:</div>
<ul>
    <li>
        <a href="/files/hwmscripts/hwm.armyset.user.js">Набор Армий</a>
    </li>
    <li>
        <a href="/files/hwmscripts/hwm.noroulete.user.js">Антирулетка</a>
    </li>
    <li>
        <a href="/files/hwmscripts/hwm.fine_hero_image.user.js">"Кукла" героя</a>
    </li>
    <li>
        <a href="/files/hwmscripts/hwm.price_per_fight.user.js">Цена за бой</a>
    </li>
    <li>
        <a href="/files/hwmscripts/hwm.use_kb.user.js">Горячие клавиши</a>
    </li>
    <li>
        <a href="/files/hwmscripts/hwm.cyfral_strength.user.js">Прочность артефактов</a>
    </li>
</ul>

<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25.02.2016
 * Time: 1:49
 */ 