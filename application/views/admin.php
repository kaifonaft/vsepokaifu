<!doctype html>
<html>
<head>
    <meta charset="utf-8"/>
    <title><?= @$title ? $title : 'germes'?></title>
    <? if(!empty($styles) && is_array($styles)):?>
        <? foreach($styles as $path): ?>
            <link rel="stylesheet" href="<?= $path ?>"/>
        <? endforeach; ?>
    <? endif; ?>
</head>
<body>
<div class="wrapper">
    <div class="content-wrap">
        <div class="admin-head">
            <ul class="admin-menu nav nav-pills">
                <li class="hwm-top-menu-item"><a class="hwm-top-menu-ref" href="/admin">Панель</a></li>
                <li class="hwm-top-menu-item"><a class="hwm-top-menu-ref" href="/">На сайт</a></li>
                <li class="hwm-top-menu-item"><a class="hwm-top-menu-ref" href="/admin/hwm/item/">Артефакты</a></li>
                <li class="hwm-top-menu-item"><a class="hwm-top-menu-ref" href="/admin/blog/">Статьи</a></li>
            </ul>
            <div class="admin-logout">
                <?=$this->session->userdata('username')?> <a class="hwm-top-menu-ref" href="/auth/logout">Выйти</a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="admin-content">
            <?= $content ?>
        </div>
    </div>
    <div class="footer">
        <div class="copy">
            <strong>&copy; kaifonaft</strong>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/source/js/admin.js" type="text/javascript"></script>
<script src="/vendor/ckeditor/ckeditor/ckeditor.js"></script>
</body>
</html>

