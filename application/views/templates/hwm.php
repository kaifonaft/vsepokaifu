<!doctype html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8"/>
    <title><?= $title ?></title>
    <link rel="shortcut icon" href="/source/img/faviconhwm.ico" type="image/x-icon">
    <meta name="keywords" content="герои онлайн, справочник, артефакты" />
    <meta name="description" content="Герои войны и денег. Справочники и скрипты" />
    <? if(!empty($styles) && is_array($styles)):?>
        <? foreach($styles as $path): ?>
            <link rel="stylesheet" href="<?= $path ?>"/>
        <? endforeach; ?>
    <? endif; ?>
</head>
<body>
        <div class="content">
            <div class="bg-left">
                <div class="bg-right">
                    <?
                    $CI = &get_instance();
                    $CI->load->view('hwm_top_menu');
                    ?>
            <div class="content-inner"><?= !empty($content) ? $content : '' ?></div>
            <div class="footer-holder"></div>
        </div>
    </div>
        </div>
<div class="footer">
    <strong>&copy;&nbsp;kaifonaft</strong>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="/source/js/jquery.imagemapster.min.js"></script>
<script src="/source/js/main.js"></script>
</body>
</html>

