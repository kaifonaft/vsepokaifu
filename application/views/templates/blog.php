<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Всё по кайфу</title>
    <link rel="stylesheet" href="/source/css/main.css"/>
    <link rel="shortcut icon" href="/source/img/favicon.ico" type="image/x-icon">
    <link rel="icon" type="image/png" href="/source/img/favicon64x64.png" sizes="64x64">
    <link rel="icon" type="image/png" href="/source/img/favicon96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/source/img/favicon144x144.png" sizes="144x144">
</head>
<body>
    <div class="container">
        <div class="header">
            <ul class="menu">
                <li class="menu-item menu-item--main">
                    <a href="/" title="Главная"><img src="/source/img/logo-header.png" alt="Всё по кайфу!"/></a>
                </li>
                <li class="menu-item">
                    <a href="/pages/view/games-2-players">Игры для 2ух игроков</a>
                </li>
                <li class="menu-item">
                    <a href="<?= str_replace("://", "://hwm.",base_url())?>">ГВД</a>
                </li>
            </ul>
        </div>
        <div class="content">
            <?=$content?>
        </div>
        <div class="footer">
            <div class="footer-copy">&copy kaifonaft</div>
        </div>
    </div>
</body>
</html>