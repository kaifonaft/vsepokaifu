<ul class="hwm-top-menu">
    <?
    $menuLinks = [
        ['/','Скрипты'],['/map', 'Карта артефактов'],
        ['/calcOA','Подсчёт ОА'], ['/itemlib', 'Библиотека артефактов']
    ];
    foreach($menuLinks as $menuItem):
        ?><li class="hwm-top-menu-item"><?
        $url = $menuItem[0];
        $name = $menuItem[1];
        if($_SERVER['REQUEST_URI'] == $url):
            ?><span class="hwm-top-menu-ref hwm-top-menu-ref--selected"><?=$name?></span><?
        else:
            ?><a class="hwm-top-menu-ref" href="<?=$url?>"><?=$name?></a><?
        endif;
        ?></li><?
    endforeach;?>
</ul>
