<ul class="">
    <li role="presentation"><a href="/admin/hwm/item/">Картинками</a></li>
    <li role="presentation" class="disabled"><span class="navbar-link">Таблицей</span></li>
</ul>
<a class="btn btn-primary button-add" href="/admin/hwm/item/add">
    <span class="glyphicon glyphicon-plus icon" aria-hidden="true"></span>Добавить</a>
<table class="table">
    <tr>
        <th class="list-id"><a href="<?=$sortArrays['id']['link']?>">Id <span class="glyphicon <?=$sortArrays['id']['icon']?> icon"></span></a></th>
        <th class="list-actions">Действия</th>
        <th><a href="<?=$sortArrays['name']['link']?>">Название <span class="glyphicon <?=$sortArrays['name']['icon']?> icon"></span></a></th>
    </tr>
    <? foreach($items as $item): ?>
        <tr>
            <td><?= $item->id ?></td>
            <td>
                <a href="/admin/hwm/item/edit/<?= $item->id ?>" title="Редактировать"><span class="glyphicon glyphicon-pencil"></span></a>
                <a href="/admin/hwm/item/delete" title="Удалить" class="js-item-remove" data-id="<?= $item->id ?>"><span class="glyphicon glyphicon-remove icon-remove"></span></a>
            </td>
            <td><?= $item->name ?></td>
        </tr>
    <? endforeach; ?>
</table>
<a class="btn btn-primary button-add" href="/admin/hwm/item/add">
    <span class="glyphicon glyphicon-plus icon" aria-hidden="true"></span>Добавить</a>
<div class="clear"></div>