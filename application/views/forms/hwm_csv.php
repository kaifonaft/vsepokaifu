<p class="bg-success"><?=@$messages?></p>
<?=form_open('', array('class'=>'form container-fluid', 'enctype' => "multipart/form-data"))?>
    <div class="form-group">
        <label for="exampleInputFile">CSV файл</label>
        <input type="file" id="exampleInputFile" name="file">
        <p class="help-block">
            1 строка - заголовок, в ней должны быть имена колонок в базе: <? implode(',', $columns)?>.<br>
            Кодировка - utf-8. Разделитель - точка с запятой ';'
        </p>
    </div>
    <div class="clear"></div>
    <div class="form-group">
        <button type="submit" class="btn btn-default">Загрузить</button>
    </div>
<?=form_close()?>