<?=form_open('', array('class'=>'form container-fluid', 'enctype' => 'multipart/form-data'), array('id' => @$id))?>
    <div class="form-group">
        <label for="name_input">Название</label>
        <input type="text" id="name_input" class="std-input form-control" name="name"
               value="<?=@htmlspecialchars($name)?>">
    </div>
    <div class="form-group">
        <label for="machine_name_input">Транслитное название</label>
        <input type="text" id="machine_name_input" class="std-input form-control" name="machine_name"
               value="<?=@htmlspecialchars($machine_name)?>">
    </div>
    <div class="form-group">
        <label for="durability_input">Прочность</label>
        <input type="text" id="durability_input" class="std-input form-control" name="durability"
               value="<?=@htmlspecialchars($durability)?>">
    </div>
    <div class="form-group">
        <label for="img_input">Изображение</label>
        <? if($img): ?>
        <div>
            <input type="checkbox" name="img" id="remove_img" value="1"/><label for="remove_img">Удалить</label>
        </div>
        <? endif; ?>
        <input type="file" id="img_input" class="" name="img">
        <img class="form-img" src="<?=$img?>">
    </div>
    <div class="form-group">
        <label for="points_ammunition_input">Очки аммуниции</label>
        <input type="text" id="points_ammunition_input" class="std-input form-control" name="points_ammunition"
               value="<?=@htmlspecialchars($points_ammunition)?>">
    </div>
    <div class="form-group">
        <label for="price_input">Цена</label>
        <input type="text" id="price_input" class="std-input form-control" name="price"
               value="<?=@htmlspecialchars($price)?>">
    </div>
    <div class="form-group">
        <label for="min_lvl_input">Минимальный уровень</label>
        <input type="text" id="min_lvl_input" class="std-input form-control" name="min_lvl"
               value="<?=@htmlspecialchars($min_lvl)?>">
    </div>

    <div class="form-group">
        <label for="slot_input">Слот</label>
        <select id="slot_input" class="form-control std-select" name="slot">
            <option value="">-</option>
            <? foreach($slot_types as $slot_type): ?>
                <? if($slot == $slot_type['id']):?>
                    <option value="<?= $slot_type['id'] ?>"
                            selected="selected"><?=htmlspecialchars($slot_type['name'])?></option>
                <? else: ?>
                    <option value="<?= $slot_type['id'] ?>"><?=htmlspecialchars($slot_type['name'])?></option>
                <? endif; ?>
            <? endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="description_input">Описание</label>
        <textarea id="description_input" class="form-control" name="description"><?=
            @htmlspecialchars($description)
        ?></textarea>
    </div>
    <div class="clear"></div>
    <div class="form-group">
        <button type="submit" class="btn btn-default"><?=$submit_button?></button>
    </div>
<?=form_close()?>