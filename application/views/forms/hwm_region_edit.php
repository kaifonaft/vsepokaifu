<?=form_open('', array('class'=>'form container-fluid'), array('id' => @$id))?>
    <div class="form-group">
        <label for="name_input">Название</label>
        <input type="text" id="name_input" class="std-input form-control" name="name"
               value="<?=@htmlspecialchars($name)?>">
    </div>
    <div class="form-group">
        <label for="machine_name_input">Транслитное название</label>
        <input type="text" id="machine_name_input" class="std-input form-control" name="machine_name"
               value="<?=@htmlspecialchars($machine_name)?>">
    </div>
    <div class="form-group">
        <label for="coord_x_input">Координата X</label>
        <input type="text" id="coord_x_input" class="std-input form-control" name="coord_x"
               value="<?=@htmlspecialchars($coord_x)?>">
    </div>
    <div class="form-group">
        <label for="coord_y_input">Координата Y</label>
        <input type="text" id="coord_y_input" class="std-input form-control" name="coord_y"
               value="<?=@htmlspecialchars($coord_y)?>">
    </div>
    <div class="clear"></div>
    <div class="form-group">
        <button type="submit" class="btn btn-default"><?=$submit_button?></button>
    </div>
<?=form_close()?>