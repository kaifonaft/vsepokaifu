<?=form_open('', array('class'=>'form container-fluid', 'enctype' => 'multipart/form-data'), array('id' => @$id))?>
    <div class="form-group">
        <label for="name_input">Название</label>
        <input type="text" id="name_input" class="std-input form-control" name="name"
               value="<?=@htmlspecialchars($name)?>">
    </div>
    <div class="form-group">
        <label for="machine_name_input">Транслитное название</label>
        <input type="text" id="machine_name_input" class="std-input form-control" name="translit"
               value="<?=@htmlspecialchars($translit)?>">
    </div>
    <div class="form-group">
        <label for="content_input">Содержимое</label>
        <textarea id="content_input" class="form-control" name="content"><?=
            @htmlspecialchars($content)
        ?></textarea>
    </div>
    <div class="clear"></div>
    <div class="form-group">
        <button type="submit" class="btn btn-default"><?=$submit_button?></button>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
            CKEDITOR.replace( 'content_input' );
        });
    </script>
<?=form_close()?>