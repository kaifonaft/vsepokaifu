<?=form_open('', array('class'=>'form container-fluid'), array('id' => @$id))?>
    <div class="form-group">
        <label for="name_input">Название</label>
        <input type="text" id="name_input" class="std-input form-control" name="name"
               value="<?=@htmlspecialchars($name)?>">
    </div>
    <div class="form-group">
        <label for="obj_id_input">Id предприятия</label>
        <input type="text" id="obj_id_input" class="std-input form-control" name="obj_id"
               value="<?=@htmlspecialchars($obj_id)?>">
    </div>
    <div class="form-group">
        <label for="coord_x_input">Объект(артефакт, ресурс)</label>
        <input type="text" id="coord_x_input" class="std-input form-control" name="coord_x"
               value="<?=@htmlspecialchars($item_id)?>">
    </div>
    <div class="form-group">
        <label for="region_id_input">Регион</label>
        <input type="text" id="region_id_input" class="std-input form-control" name="region_id"
               value="<?=@htmlspecialchars($region_id)?>">
    </div>
    <div class="clear"></div>
    <div class="form-group">
        <button type="submit" class="btn btn-default"><?=$submit_button?></button>
    </div>
<?=form_close()?>