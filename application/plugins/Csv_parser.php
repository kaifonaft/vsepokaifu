<?php
/**
 * Parse csv and insert it to database
 */
class Csv_parser
{
    var $CI;
    private $tablename = '';
    private $columns = array();
    private $searchColumn = '';
    private $db;

    public  function __construct($tablename = '', $columns = array(), $searchColumn = '')
    {
        $this->CI =& get_instance();
        $this->tablename = $tablename;
        $this->columns = $columns;
        $this->searchColumn = $searchColumn;
        $this->db = $this->CI->db;

        log_message('debug', "Csv_parser Class Initialized");
    }

    // --------------------------------------------------------------------

    /**
     * @param string $filepath absoulte file path
     * @return boolean true if parse success
    */
    public function parseFile($filePath){
        try{
            $fileHandle = fopen($filePath, 'r');
            if(!$fileHandle){
                return false;
            }
            $headerRow = fgetcsv($fileHandle, 300, ';');
            $columns = $this->columns;
            $tablename = $this->tablename;
            $searchColumn = $this->searchColumn;
            $db = $this->db;
            $name2num = array();
            $existingColumns = array();
            foreach($headerRow as $columnNum => $columnName){
                if(in_array($columnName, $columns)){
                    $name2num[$columnName] = $columnNum;
                    $existingColumns []= $columnName;
                }
            }
            while($rowAr = fgetcsv($fileHandle, 300, ';')){
                $dbData = array();
                foreach($existingColumns as $column){
                    $dbData[$column] = $rowAr[$name2num[$column]];
                }
                if(!empty($searchColumn)){
                    $dbRow = $db->get_where($tablename, array($searchColumn => $dbData[$searchColumn]));
                    if(!empty($dbRow) && $dbRow->num_rows > 0){
                        $db->set($dbData)
                            ->where($searchColumn, $dbData[$searchColumn])
                            ->update($tablename);
                    }else{
                        $db->insert($tablename, $dbData);
                    }
                }else{
                    $db->insert($tablename, $dbData);
                }
            }
        }catch(Exception $e){
            return false;
        }
        fclose($fileHandle);
        return true;

    }

}