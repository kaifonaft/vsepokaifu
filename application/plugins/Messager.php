<?php
/**
 * CodeIgniter Calendar Class
 *
 * This class enables the creation of calendars
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/calendar.html
 */
class Messager
{
    var $CI;
    private static $instance = null;
    private $messages = array();

    /**
     * Constructor
     *
     * Loads the calendar language file and sets the default time reference
     */
    private function __construct($config = array())
    {
        $this->CI =& get_instance();

        if (count($config) > 0) {
            $this->initialize($config);
        }

        log_message('debug', "Messager Class Initialized");
    }

    // --------------------------------------------------------------------

    /**
     * Initialize the user preferences
     *
     * Accepts an associative array as input, containing display preferences
     *
     * @access    public
     * @param    array    config preferences
     * @return    void
     */
    public function initialize($config = array())
    {
        foreach ($config as $key => $val) {
            if (isset($this->$key)) {
                $this->$key = $val;
            }
        }
    }

    public static function getInstance($config = array()){
        if(empty(static::$instance)){
            static::$instance = new Messager($config);
        }
        return static::$instance;
    }

    public function addMessage($text){
        array_push($this->messages, $text);
    }

    public function getAllMessages(){
        return $this->messages;
    }

    public function renderMessages($view = ''){
        if(empty($view)){
            $view = 'forms/messages';
        }
        $data = array(
            'messages' => $this->getAllMessages(),
        );
        $htmlMessages = $this->CI->load->view($view, $data, true);
        return $htmlMessages;
    }
}