<?php
class File{
    public static function remove($path){
        if(file_exists($path)){
            unlink($path);
        }
    }

    /** @param $file item from $_FILES */
    public static function upload($tmp_name, $dir, $name){
        $path = rtrim($_SERVER['DOCUMENT_ROOT'],'/').'/'.trim($dir, '/').'/'.$name;
        self::remove($path);
        return move_uploaded_file($tmp_name, $path);
    }
}