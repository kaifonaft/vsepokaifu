<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11.07.2017
 * Time: 12:43
 */
class SortLinks
{
    protected $validSort, $defaultSort;
    protected $defaultDir = 'asc', $validDir = array('asc', 'desc');
    public function __construct($validSort, $defaultSort = null){
        $this->validSort = $validSort;
        $this->defaultSort = $defaultSort;
    }

    /**
     * get params for new sort link from current sort params
     * return url params for example array('sort' => 'id', 'dir' => 'desc')
     * @param $curSort
     * @param $curDir
     * @param $newSort
     * @return array
     */
    public function getSortParams($curSort, $curDir, $newSort){
        $curDir = strtolower($curDir);
        if(!in_array($curDir, $this->validDir)){
            $curDir = $this->defaultDir;
        }
        if(!in_array($curSort, $this->validSort)){
            $curSort = $this->defaultSort;
        }
        if(!in_array($newSort, $this->validSort)){
            $newSort = $this->defaultSort;
        }
        if($curSort == $newSort){
            $newDir = $this->mirrorDir($curDir);
        } else {
            $newDir = $this->defaultDir;
        }
        return array('sort' => $newSort, 'dir' => $newDir);
    }

    public function mirrorDir($dir){
        $dir = strtolower($dir);
        if($dir == 'desc'){
            return 'asc';
        } else {
            return 'desc';
        }
    }

    /**
     * @param string $curSort current sort param
     * @param string $curOrder current sort order
     * @param string $newSort new sort param
     * @return string
     */
    public function getSortLink($curSort, $curOrder, $newSort){
        $params = $this->getSortParams($curSort, $curOrder, $newSort);
        if($params['dir'] == $this->defaultDir){
            $params['dir'] = null;
        }
        if($params['sort'] == $this->defaultSort){
            $params['sort'] = null;
        }

        return http_build_query($params);
    }

    public function getSortOrDefault($curSort){
        if(!in_array($curSort, $this->validSort)){
            $curSort = $this->defaultSort;
        }
        return $curSort;
    }

    public function getDirOrDefault($curDir){
        if(!in_array($curDir, $this->validDir)){
            $curDir = $this->defaultDir;
        }
        return $curDir;
    }
}